var fs = require('fs');
var xml2js = require('xml2js');
var ngConfig = require('gulp-ng-config');
var config = require('./config.js');
var jeditor = require('gulp-json-editor');

var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');

/* Configure files for environment */
if (config.env === 'prod') {
    config.ENVIRONMENT = 'P';
    gutil.log(gutil.colors.bold('Configuring app for production'));
} else if (config.env === 'demo') {
    config.ENVIRONMENT = 'D';
    gutil.log(gutil.colors.bold('Configuring app for demo'));
} else {
    config.ENVIRONMENT = 'X';
    gutil.log(gutil.colors.bold('Configuring app for development'));
}

fs.writeFileSync('./env.json', JSON.stringify(config));
gulp.src('./env.json')
        .pipe(ngConfig('environment.config', {
            constants: config,
            createModule: false
        }))
        .pipe(gulp.dest('./www/'));

var parser = new xml2js.Parser();
var builder = new xml2js.Builder({
    renderOpts: {
        pretty: true,
        indent: '    '
    },
    xmldec: {
        version: '1.0',
        encoding: 'utf-8'
    }
});

// read & parse config.xml file
parser.parseString(fs.readFileSync('config.xml'), function (err, result) {
    if (err) throw err;
    if (config.env === 'demo') {
        if (!result.widget.name[0].match(/DEMO/g))
            result.widget.name = 'DEMO ' + result.widget.name;
        if (!result.widget.$.id.match(/\.demo/gi))
            result.widget.$.id += '.demo';
    } else {
        result.widget.name = result.widget.name[0].replace("DEMO ", "");
        result.widget.$.id = result.widget.$.id.replace(".demo", "");
    }
    if (!!config.appVersion) {
        result.widget.$.version = config.appVersion;
    }
    // write file
    fs.writeFileSync('config.xml', builder.buildObject(result));
});

if (!!config.appVersion) {
    gulp.src('./package.json')
            .pipe(jeditor({
                "version": config.appVersion
            }))
            .pipe(gulp.dest('./'));
    gulp.src('./package-lock.json')
            .pipe(jeditor({
                "version": config.appVersion
            }))
            .pipe(gulp.dest('./'));
}
/* ~ */

var paths = {
    sass: ['./scss/**/*.scss']
};

gulp.task('serve:before', ['default','watch']);
gulp.task('default', ['sass']);

gulp.task('sass', function(done) {
    gulp.src('./scss/ionic.app.scss')
            .pipe(sass())
            .on('error', sass.logError)
            .pipe(gulp.dest('./www/css/'))
            .pipe(minifyCss({
                keepSpecialComments: 0
            }))
            .pipe(rename({ extname: '.min.css' }))
            .pipe(gulp.dest('./www/css/'))
            .on('end', done);
});

gulp.task('watch', ['sass'], function() {
    gulp.watch(paths.sass, ['sass']);
});

gulp.task('install', ['git-check'], function() {
    return bower.commands.install()
            .on('log', function(data) {
                gutil.log('bower', gutil.colors.cyan(data.id), data.message);
            });
});

gulp.task('git-check', function(done) {
    if (!sh.which('git')) {
        console.log(
                '  ' + gutil.colors.red('Git is not installed.'),
                '\n  Git, the version control system, is required to download Ionic.',
                '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
                '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
        );
        process.exit(1);
    }
    done();
});
