/**
 * Created by gcuellar on 28/11/17.
 */
var environments = {
    appVersion: "3.3.1",
    env: 'dev', // Use prod, demo or dev (other values will default to dev)
    REST_OAUTH_SECRET: 'ABCD1234',
    //REST_ENDPOINT: 'http://192.168.15.69:8085/',
    REST_ENDPOINT: 'http://209.126.99.20:8085/',
};

module.exports = environments;