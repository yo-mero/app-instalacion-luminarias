app.controller('IndexController',
['$scope', '$state', '$ionicPlatform', 'loadingScreen', 'AppServices',
    function ($scope, $state, $ionicPlatform, loadingScreen, AppServices) {
        $scope.goTo = function (module) {
            loadingScreen.show();
            switch (module) {
                case 'installation':
                    AppServices.sync().then(function (response) {
                        loadingScreen.hide();
                        $state.go('app.installation', {idGroup: response.idGroup});
                    }, function (response) {
                        loadingScreen.hide();
                        if (response.confirm) // Retry
                            $scope.goTo('installation');
                        else if (response.hasData) // Go anyway
                            $state.go('app.installation', {idGroup: response.idGroup});
                        else // Cancel/Close
                            ionic.Platform.exitApp();
                    });
                    break;
            }
        };
        $ionicPlatform.ready(function () {
            $scope.goTo('installation');
        });
    }
]);