var app = angular.module('app.common.alert', []);

app.service('Alert', function($ionicPopup, DEVMODE){
  this.alert = function(message, respCode, respMessage) {
      var showAlert = function() {
          var alertPopup = $ionicPopup.alert({
              title: '',
              template: '<div style="text-align: center">' + message + (respMessage != "" ? '<br><br>' + respMessage : "") + (DEVMODE ? (respCode != "" ? '<br>' + respCode : "") : "") + '</div>',
          });
      };
      showAlert();
  }
});

app.service('Loading', function($ionicLoading, $timeout) {
    
	this.show = function(timeout) {
        $ionicLoading.show({
		    noBackdrop: true,
        });

		// wait "timeout" seconds and hide the overlay
        $timeout(function() {
            $ionicLoading.hide();
        }, timeout);
    };
	
    this.hide = function() {
        $ionicLoading.hide();
    };
 
});