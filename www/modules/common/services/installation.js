angular.module('app.common.service.installation', [])
        .service('Install', ['$rootScope', '$q', '$interval', '$log', 'camelCase', 'RestService', 'DBService', 'Group', 'Positions', 'Lamps',
            function ($rootScope, $q, $interval, $log, camelCase, RestService, DBService, Group, Positions, Lamps) {
                var selfObjArray = [], promises = [], factoryName = 'Installation', self = {};
                self.statusInstall = {
                    STANDBY: "PENDIENTE",
                    SENDING: "ENVIANDO",
                    ERROR: "ERROR",
                    SAVED: "GUARDADO"
                };

                self.init = function (sessionRowVersion) {
                    var deferPromise = $q.defer();

                    DBService.DB_action([], factoryName, 'GETALL').then(function (result) {
                        result = DBService.getResult(result);
                        for (var i = 0; i < result.length; i++) {
                            selfObjArray[i] = {};
                            for (var property in result[i]) {
                                selfObjArray[i][camelCase.fromString(property)] = result[i][property];
                            }
                        }

                        var syncRequired = false;
                        for (var i = 0; i < selfObjArray.length && !syncRequired; i++) {
                            if (selfObjArray[i].rowVersion == "undefined" || !selfObjArray[i].rowVersion)
                                continue;

                            if (selfObjArray[i].rowVersion > sessionRowVersion || selfObjArray[i].status != self.statusInstall.SAVED)
                                syncRequired = true;
                        }

                        var syncPromise;
                        if (syncRequired) {
                            syncPromise = self.Synchronize().then(function () {
                                $log.info('Stand by installations saved on server');
                            }, function () {
                                if (!angular.isDefined($rootScope.trySyncrhonize))
                                    $rootScope.trySyncrhonize = $interval(self.Synchronize, 5 * 60000);
                                $log.error('Error while saving installations on server, trying again in 5 minutes');
                            });
                        }
                        $q.all([syncPromise]).finally(function () {
                            var today = new Date();
                            today.setHours(3, 0, 0, 0);
                            promises = [];
                            if (new Date().getTime() > today.getTime() && sessionRowVersion < today.getTime()) {
                                if (!!selfObjArray.length) {
                                    var oldInstalledAndSync = selfObjArray.filter(findInstalledAndSycAndRowVersionBefore, today.getTime());
                                    for (var i = 0; i < oldInstalledAndSync.length; i++) {
                                        promises.push(self.removeSingle(oldInstalledAndSync[i].id));
                                        promises.push(Positions.removeSingle(oldInstalledAndSync[i].idPosition));
                                        promises.push(Lamps.removeSingle(oldInstalledAndSync[i].idLamp));
                                    }

                                    function findInstalledAndSycAndRowVersionBefore(installation) {
                                        return installation.status == self.statusInstall.SAVED && installation.sync == 1 && installation.rowVersion < this;
                                    }
                                }
                            }
                            $q.all(promises).finally(function () {
                                promises = [];

                                if (!selfObjArray.length) {
                                    var oldInstalledPositions = Positions.getAll().filter(findInstalledAndRowVersionBefore, today.getTime());
                                    if (!!oldInstalledPositions.length)
                                        for (var i = 0; i < oldInstalledPositions.length; i++)
                                            promises.push(Positions.removeSingle(oldInstalledPositions[i].id));
                                    var oldInstalledLamps = Lamps.getAll().filter(findInstalledAndRowVersionBefore, today.getTime());
                                    if (!!oldInstalledLamps.length)
                                        for (var i = 0; i < oldInstalledLamps.length; i++)
                                            promises.push(Lamps.removeSingle(oldInstalledLamps[i].id));

                                    function findInstalledAndRowVersionBefore(obj) {
                                        if (typeof obj.installed == "string") obj.installed = obj.installed == "true";
                                        return (obj.installed == 1 || obj.installed == true) && obj.rowVersion < this;
                                    }
                                }

                                $q.all(promises).finally(function () {
                                    deferPromise.resolve();
                                });
                            });
                        });
                    });

                    return deferPromise.promise;
                };

                self.filterStatus = function (Installation) {
                    return Installation.status == this.valueOf();
                };

                self.getAll = function () {
                    // Get all
                    return selfObjArray;
                };

                self.getSingle = function (elementForFilter) {
                    for (var i = 0; i < selfObjArray.length; i++) {
                        if (typeof elementForFilter == "object") {
                            if (elementForFilter.hasOwnProperty('group') && Number(selfObjArray[i].idGroup) == Number(elementForFilter.group)) {
                                if (elementForFilter.hasOwnProperty('position') && Number(selfObjArray[i].idPosition) == Number(elementForFilter.position))
                                    return selfObjArray[i];
                                else if (elementForFilter.hasOwnProperty('installed') && selfObjArray[i].installedDate == elementForFilter.installed)
                                    return selfObjArray[i];
                            }
                        } else {
                            if (selfObjArray[i].id == elementForFilter)
                                return selfObjArray[i];
                        }
                    }
                    return null;
                };

                self.getLinkedInstallations = function (Installation) {
                    var linkedInstallations = [];
                    for (var i = 0; i < selfObjArray.length; i++) {
                        if (!!selfObjArray[i].linked && selfObjArray[i].idGroup == Installation.idGroup && selfObjArray[i].installedDate == Installation.installedDate)
                            linkedInstallations.push(selfObjArray[i]);
                    }
                    return linkedInstallations
                };

                self.getLastInstallation = function () {
                    if (selfObjArray.length > 0) {
                        var lastID = parseInt(Math.max.apply(Math, selfObjArray.map(function (obj) {
                            return obj.id;
                        })));

                        function installById(install) {
                            return install.id == this;
                        }

                        return selfObjArray.find(installById, lastID) || selfObjArray[selfObjArray.length - 1];
                    }
                    return null;
                };

                self.Synchronize = function () {
                    var deferPromise = $q.defer();

                    var installations = selfObjArray.filter(self.filterStatus, self.statusInstall.STANDBY);
                    installations = installations.concat(selfObjArray.filter(self.filterStatus, self.statusInstall.ERROR));
                    promises = [];

                    try {
                        for (var i = 0; i < installations.length; i++) {
                            promises.push(self.save(installations[i]));
                        }
                        $q.all(promises).then(function () {
                            promises = [];

                            if (angular.isDefined($rootScope.trySyncrhonize)) {
                                $interval.cancel($rootScope.trySyncrhonize);
                                $rootScope.trySyncrhonize = undefined;
                            }

                            deferPromise.resolve();
                        }, function (error) {
                            deferPromise.reject(error);
                        });
                    } catch (e) {
                        $log.error("Falló envio de la instalación", e);
                        deferPromise.reject(e);
                    }

                    return deferPromise.promise;
                };

                self.save = function (installationData) {
                    // Save in WS
                    var deferPromise = $q.defer();

                    var indexInSelfObjArray = selfObjArray.indexOf(installationData);

                    var dbObj = {};
                    for (var property in installationData) {
                        dbObj[camelCase.toSeparate(property)] = installationData[property];
                    }

                    dbObj.status = self.statusInstall.SENDING;
                    DBService.DB_action(dbObj, factoryName, 'UPDATE').then(function () {
                        if (!installationData.sync)
                            installationData.id = null;
                        installationData.active = 1;
                        var objGroup = {group: Group.getSingle(installationData.idGroup)},
                            objPosition = {position: angular.extend({}, Positions.getSingle(463), objGroup)},
                            objLamp = {lamp: angular.extend({}, Lamps.getSingle(716), objGroup)};
                        RestService.sendRequest(
                                angular.extend({},
                                        installationData,
                                        objGroup,
                                        objPosition,
                                        objLamp,
                                        {idPosition: 463, idLamp: 716}
                                ), 'InstallPOST', '').then(function (response) {
                            installationData.id = dbObj.id;
                            DBService.DB_action({id: installationData.id}, factoryName, 'INSTALLED').finally(function () {
                                DBService.DB_action([response, installationData.id], factoryName, 'CHANGEID').then(function () {
                                    DBService.DB_action({id: response}, factoryName, "GETBYID").then(function (result) {
                                        result = DBService.getResult(result);
                                        for (var property in result) {
                                            selfObjArray[indexInSelfObjArray][camelCase.fromString(property)] = result[property];
                                        }
                                        deferPromise.resolve(response);
                                    }, function () {
                                        installationData.id = response;
                                        installationData.sync = 1;
                                        installationData.status = self.statusInstall.SAVED;
                                        selfObjArray[indexInSelfObjArray] = installationData;
                                        deferPromise.resolve(response);
                                    });
                                }, function (error) {
                                    dbObj.sync = 0;
                                    DBService.DB_action(dbObj, factoryName, 'UPDATE');
                                    deferPromise.reject(error);
                                });
                            });
                        }, function (error) {
                            installationData.id = dbObj.id;
                            installationData.status = self.statusInstall.ERROR;
                            dbObj.status = installationData.status;
                            DBService.DB_action(dbObj, factoryName, 'UPDATE');
                            switch (error.code) {
                                case "ERR409":
                                    RestService.sendRequest({
                                        move: factoryName,
                                        json: angular.toJson(installationData),
                                        message: error.message
                                    }, 'ConflictPOST', '').then(function () {
                                        DBService.DB_action({id: installationData.id}, factoryName, 'INSTALLED');
                                    });
                                    deferPromise.resolve('Lampara registrada como instalada');
                                    break;
                                default:
                                    deferPromise.reject(error);
                                    break;
                            }
                        });
                    }, function (error) {
                        deferPromise.reject(error);
                    });

                    return deferPromise.promise;
                };

                self.setAll = function (newObjArray, fromWS) {
                    var deferPromise = $q.defer();
                    promises = [];
                    // Set one by one
                    for (var i = 0; i < newObjArray.length; i++) {
                        promises.push(self.setSingle(newObjArray[i], !!fromWS));
                    }

                    $q.all(promises).then(function () {
                        deferPromise.resolve()
                    }, function (error) {
                        deferPromise.reject(error);
                    });

                    return deferPromise.promise;
                };

                self.setSingle = function (newObj, fromWS) {
                    var deferPromise = $q.defer();

                    var installation = null;
                    if (!!newObj.id)
                        installation = self.getSingle(newObj.id);
                    else
                        installation = self.getSingle({group: newObj.idGroup, position: newObj.idPosition});

                    if (!!fromWS) {
                        newObj.status = self.statusInstall.SAVED;
                        newObj.photoBefore = '';
                        newObj.photoAfter = '';
                        newObj.sync = 1;
                    }

                    var dbObj = {};
                    for (var property in newObj) {
                        switch (property) {
                            case 'group':
                            case 'position':
                            case 'lamp':
                                var newProperty = "id_" + camelCase.toSeparate(property);
                                if (!newObj.hasOwnProperty(camelCase.fromString(newProperty))) {
                                    dbObj[newProperty] = newObj[property].id;
                                    newObj[camelCase.fromString(newProperty)] = dbObj[newProperty];
                                }
                                break;
                            default:
                                dbObj[camelCase.toSeparate(property)] = newObj[property] == "null" ? null : newObj[property];
                        }
                    }

                    if (installation !== null) {
                        // Found, update it
                        newObj.id = installation.id;
                        dbObj.id = newObj.id;
                        if (!fromWS)
                            newObj.sync = installation.sync;
                        dbObj.sync = newObj.sync

                        selfObjArray[selfObjArray.indexOf(installation)] = newObj;
                        DBService.DB_action(dbObj, factoryName, "UPDATE").then(function () {
                            deferPromise.resolve();
                        }, function () {
                            deferPromise.reject();
                        });
                    } else {
                        // Not found, insert it
                        newObj.sync = +(!!newObj.sync);
                        dbObj.sync = newObj.sync;

                        selfObjArray.push(newObj);
                        DBService.DB_action(dbObj, factoryName, "INSERT").then(function () {
                            deferPromise.resolve();
                        }, function () {
                            deferPromise.reject();
                        });
                    }

                    return deferPromise.promise;
                };

                self.removeSingle = function (id) {
                    var deferPromise = $q.defer();

                    var objToRemove = self.getSingle(id);
                    if (objToRemove != null) {
                        DBService.DB_action(objToRemove, factoryName, "DELETE").then(function () {
                            selfObjArray.splice(selfObjArray.indexOf(objToRemove), 1);
                            deferPromise.resolve();
                        }, function () {
                            deferPromise.reject();
                        });
                    }

                    return deferPromise.promise;
                };

                self.clear = function () {
                    // Erase all
                    var deferPromise = $q.defer();

                    selfObjArray = [];
                    DBService.DB_action(newObj, factoryName, "DELETEALL").then(function () {
                        deferPromise.resolve();
                    }, function () {
                        deferPromise.reject();
                    });

                    return deferPromise.promise;
                };

                return self;
            }
        ]);