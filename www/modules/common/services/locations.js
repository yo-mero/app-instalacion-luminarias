/**
 * Created by gcuellar on 13/12/17.
 */
angular.module('app.common.service.locations', [])
        .service('Locations', ['$rootScope', '$q', '$interval', '$timeout', '$log', 'RestService', 'DBService', 'Device', 'Session',
            function ($rootScope, $q, $interval, $timeout, $log, RestService, DBService, Device, Session) {
                var self = {};
                var factoryName = "Locations";
                var intervalPromise, timeoutPromise;
                var timeoutTimeToSync = 30 * 60 * 1000; // Time for force a sync after a first location save occurs (min * sec * millisec)

                self.init = function (idDevice) {
                    $rootScope.$on('locationChange', function () {
                        self.save();
                    });
                    self.sync(idDevice);
                };

                self.sync = function (idDevice) {
                    var deferPromise = $q.defer();

                    if (timeoutPromise) {
                        $timeout.cancel(timeoutPromise);
                        timeoutPromise = undefined;
                    }
                    DBService.DB_action([], factoryName, "GETALL").then(function (result) {
                        result = DBService.getResult(result);
                        if (!!result.length) {
                            var deviceInfo = Device.Info();

                            var data = {
                                results: result,
                                app: 'Installations',
                                device: (!isNaN(idDevice) && idDevice > 0) ? idDevice : deviceInfo.manufacturer + " " + deviceInfo.model + " " + deviceInfo.serial
                            };
                            RestService.sendRequest(data, "LocationsPOST")
                                    .then(function () {
                                        DBService.DB_action([], factoryName, "DELETEALL");
                                        if (intervalPromise) {
                                            $interval.cancel(intervalPromise);
                                            intervalPromise = undefined;
                                        }

                                        deferPromise.resolve();
                                    }, function (error) {
                                        $log.error('Error while saving locations on server, trying again in 60 seconds');
                                        if (!intervalPromise)
                                            intervalPromise = $interval(self.sync, 60000, 0, true, idDevice);

                                        deferPromise.reject(error);
                                    });
                        } else
                            deferPromise.resolve();
                    });

                    return deferPromise.promise;
                };

                self.save = function () {
                    if ($rootScope.gps.validPosition) {
                        var data = {
                            date: new Date().getTime(),
                            idGroup: !!Session.getUsername() ? Session.getUsername() : null,
                            coords: JSON.stringify({lat: $rootScope.gps.latitude, lng: $rootScope.gps.longitude})
                        };
                        DBService.DB_action(data, factoryName, "INSERT").then(function (response) {
                            if (!timeoutPromise)
                                timeoutPromise = $timeout(self.sync, timeoutTimeToSync);
                            if (response.insertId >= 30)
                                self.sync();
                        });
                    }
                };

                return self;
            }
        ]);