angular.module('app.common.service.appservices', [])
    .service('AppServices', ['$rootScope', '$q', '$ionicPopup', 'loadingScreen', 'DBService', 'RestService', 'Session', 'Group', 'Positions', 'Lamps', 'Install', 'Locations',
        function ($rootScope, $q, $ionicPopup, loadingScreen, DBService, RestService, Session, Group, Positions, Lamps, Install, Locations) {
            var self = {};

            self.sync = function () {
                var deferPromise = $q.defer();
                // Sync from server
                Session.init().then(function () {
                    $rootScope.Username = Session.getUsername();
                    var sessionData = Session.getAll().slice(0).shift();
                    var promiseUpdatesByVersion = [];
                    if (sessionData.app_version != $rootScope.appVersion)
                        promiseUpdatesByVersion.push(DBService.appUpdates(sessionData.app_version, $rootScope.appVersion));

                    $q.all(promiseUpdatesByVersion).then(function (response) {
                        if (!!response.length) {
                            //App Version index = 0
                            if (sessionData.app_version != response[0]) {
                                sessionData.app_version = response[0];
                                Session.setSingle(sessionData);
                            }
                        }

                        Locations.init(sessionData.device_id_assigned);
                        RestService.sendRequest(null, 'Synchronize', "row_version=" + Session.getRowVersion()).then(function (device_id) {
                            if (sessionData.device_id_assigned != device_id) {
                                sessionData.device_id_assigned = device_id;
                                Session.setSingle(sessionData);
                            }

                            $q.all([Group.init(Session.getRowVersion()), Positions.init(), Lamps.init()]).then(function () {
                                Install.init(Session.getRowVersion()).finally(function () {
                                    deferPromise.resolve({idGroup: +Session.getUsername()});
                                })
                            }, function () {
                                onConnectionError().then(function (response) {
                                    deferPromise.reject(response);
                                });
                            });
                        }, function () {
                            onConnectionError().then(function (response) {
                                deferPromise.reject(response);
                            });
                        });
                    }, function () {
                        deferPromise.reject({confirm: false, hasData: false});
                    });
                }, function () {
                    deferPromise.reject({confirm: false, hasData: false});
                });
                return deferPromise.promise;
            };

            self.updateData = function(idGroup) {
                var deferPromise = $q.defer();

                loadingScreen.show();

                if (+Session.getUsername() != +idGroup)
                    Session.setUsername(idGroup);

                $q.all([Positions.Synchronize(idGroup, Session.getRowVersion()), Lamps.Synchronize(idGroup, Session.getRowVersion())]).then(function () {
                    Session.setRowVersion(new Date().getTime());

                    loadingScreen.hide();

                    deferPromise.resolve();
                }, function () {
                    onConnectionError().then(function (response) {
                        loadingScreen.hide();

                        deferPromise.reject(response);
                    })
                });

                return deferPromise.promise;
            };

            function onConnectionError() {
                var deferPromise = $q.defer();

                var template = 'No se pudo conectar al servidor.';
                var cancelText = 'Cerrar';

                var lastRowVersion = new Date(Session.getRowVersion());
                var yesterday = new Date(new Date().getTime() - (24 * 3600000));

                var hasData = lastRowVersion.getTime() >= yesterday.getTime();
                hasData = (new Date(Session.getRowVersion()).getTime() > 0);
                if (hasData) {
                    template = 'No se pudo actualizar la información con el servidor, pero hay información local valida.'
                    cancelText = 'Seguir'
                }

                loadingScreen.hide(); // Hide loading screen, just in case it exists, so don't mess with the Popup
                $ionicPopup.confirm({
                    title: 'No hay conexión al servidor',
                    template: template,
                    cancelText: cancelText,
                    okText: 'Reintentar'
                }).then(function (response) {
                    if (!response && hasData) {
                        $q.all([Group.init(Session.getRowVersion()), Positions.init(Session.getRowVersion()), Lamps.init(Session.getRowVersion())]).finally(function () {
                            deferPromise.resolve({confirm: false, hasData: true, idGroup: +Session.getUsername()});
                        });
                    } else
                        deferPromise.resolve({confirm: response, hasData: hasData, idGroup: +Session.getUsername()});
                });

                return deferPromise.promise;
            }

            return self;
        }
    ]);