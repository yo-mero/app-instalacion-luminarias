angular.module('app.common.service.group', [])
        .service('Group', ['$rootScope', '$q', 'camelCase', 'DBService', 'RestService',
            function ($rootScope, $q, camelCase, DBService, RestService) {
                var selfObjArray = [], promises = [], factoryName = 'Groups', self = {};
                var numRows = 0;

                self.init = function (timestampRowVersion) {
                    var deferPromise = $q.defer();

                    DBService.DB_action([], factoryName, "GETALL").then(function (result) {
                        result = DBService.getResult(result);
                        for (var i = 0; i < result.length; i++) {
                            selfObjArray[i] = {};
                            for (var property in result[i]) {
                                selfObjArray[i][camelCase.fromString(property)] = result[i][property];
                            }
                        }

                        var timestamp = 0;
                        for (numRows = 0; numRows < selfObjArray.length; numRows++) {
                            if (selfObjArray[numRows].rowVersion == "undefined" || !selfObjArray[numRows].rowVersion)
                                break;

                            if (selfObjArray[numRows].rowVersion <= timestampRowVersion && selfObjArray[numRows].rowVersion > timestamp)
                                timestamp = Math.floor(selfObjArray[numRows].rowVersion);
                        }

                        RestService.sendRequest(null, 'GroupsGET', 'row_version=' + timestamp + '&numRows=' + numRows + "&actives=true").then(function (response) {
                            if (!!response.length) {
                                self.setAll(response).then(function () {
                                    deferPromise.resolve();
                                }, function (error) {
                                    deferPromise.reject(error);
                                });
                            } else
                                deferPromise.resolve();
                        }, function (error) {
                            deferPromise.reject(error);
                        });
                    });

                    return deferPromise.promise;
                };

                self.getAll = function () {
                    // Get all
                    return selfObjArray;
                };

                self.getSingle = function (id) {
                    // Get by id
                    for (var i = 0; i < selfObjArray.length; i++) {
                        if (selfObjArray[i].id == id) {
                            return selfObjArray[i];
                        }
                    }
                    return null;
                };

                self.setAll = function (newObjArray) {
                    var deferPromise = $q.defer();
                    promises = [];
                    // Set one by one
                    for (var i = 0; i < newObjArray.length; i++) {
                        promises.push(self.setSingle(newObjArray[i]));
                    }

                    $q.all(promises).then(function () {
                        deferPromise.resolve();
                    }, function (error) {
                        deferPromise.reject(error);
                    });

                    return deferPromise.promise;
                };

                self.setSingle = function (newObj) {
                    var deferPromise = $q.defer();

                    var group = self.getSingle(newObj.id);

                    var dbObj = {};
                    for (var property in newObj) {
                        dbObj[camelCase.toSeparate(property)] = newObj[property];
                    }

                    if (group !== null) {
                        // Found, update it
                        selfObjArray[selfObjArray.indexOf(group)] = newObj;
                        DBService.DB_action(dbObj, factoryName, "UPDATE").then(function () {
                            deferPromise.resolve();
                        }, function () {
                            deferPromise.reject();
                        });
                    } else {
                        // Not found, insert it
                        selfObjArray.push(newObj);
                        DBService.DB_action(dbObj, factoryName, "INSERT").then(function () {
                            deferPromise.resolve();
                        }, function () {
                            deferPromise.reject();
                        });
                    }

                    return deferPromise.promise;
                };

                self.clear = function () {
                    // Erase all
                    var deferPromise = $q.defer();

                    selfObjArray = [];
                    DBService.DB_action([], factoryName, "DELETEALL").then(function () {
                        deferPromise.resolve();
                    }, function () {
                        deferPromise.reject();
                    });

                    return deferPromise.promise;
                };

                return self;
            }
        ]);