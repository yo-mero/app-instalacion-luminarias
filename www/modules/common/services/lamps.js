angular.module('app.common.service.lamps', [])
        .service('Lamps', ['$rootScope', '$q', '$log', 'camelCase', 'DBService', 'RestService',
            function ($rootScope, $q, $log, camelCase, DBService, RestService) {
                var selfObjArray = [], promises = [], factoryName = "Lamps", self = {};

                self.init = function () {
                    var deferPromise = $q.defer();

                    DBService.DB_action([], factoryName, "GETALL").then(function (result) {
                        result = DBService.getResult(result);
                        for (var i = 0; i < result.length; i++) {
                            selfObjArray[i] = {};
                            for (var property in result[i]) {
                                selfObjArray[i][camelCase.fromString(property)] = result[i][property];
                            }
                        }
                        deferPromise.resolve();
                    }, function (error) {
                        $log.error("No se pudo cargar la lista de luminarias: ", error);
                        deferPromise.reject(error);
                    });

                    return deferPromise.promise;
                };

                self.Synchronize = function (idGroup, sessionRowVersion) {
                    var deferPromise = $q.defer();

                    RestService.sendRequest(null, 'LampsGET', 'idGroup=' + idGroup + '&row_version=' + sessionRowVersion).then(function (response) {
                        if (!!response.length) {
                            self.setAll(response).then(function () {
                                deferPromise.resolve();
                            }, function (error) {
                                deferPromise.reject(error);
                            });
                        } else {
                            deferPromise.resolve();
                        }
                    }, function (error) {
                        $log.error("Error al sincronizar luminarias: ", error)
                        deferPromise.reject(error);
                    });

                    return deferPromise.promise;
                };

                self.getAll = function () {
                    // Get all
                    return selfObjArray;
                };

                self.getActives = function () {
                    function findActives(obj) {
                        if (typeof obj.active == "string") obj.active = obj.active == "true";
                        return (obj.active == 1 || obj.active == true);
                    }

                    return selfObjArray.filter(findActives);
                }

                self.getSingle = function (id) {
                    // Get by id
                    for (var i = 0; i < selfObjArray.length; i++) {
                        if (selfObjArray[i].id == id) {
                            return selfObjArray[i];
                        }
                    }
                    return null;
                };

                self.getByFilter = function (group, lampsOnInstallations) {
                    // Get Lamps of Move
                    function notInstalled(obj) {
                        if (!!lampsOnInstallations)
                            return obj.installed == 0 || lampsOnInstallations.indexOf(obj.id) < 0;
                        else
                            return obj.installed == 0;
                    }

                    function byGroup(obj) {
                        return obj.idGroup == group.id;
                    }

                    return self.getActives().filter(byGroup).filter(notInstalled);
                };

                self.getByWatts = function (lamps, wattsList) {
                    function findByWatts(obj) {
                        return wattsList.indexOf(obj.watts) >= 0;
                    }


                    return lamps.filter(findByWatts)
                };

                self.setAll = function (newObjArray) {
                    var deferPromise = $q.defer();
                    promises = [];
                    // Set one by one
                    for (var i = 0; i < newObjArray.length; i++) {
                        promises.push(self.setSingle(newObjArray[i]));
                    }

                    $q.all(promises).then(function () {
                        deferPromise.resolve();
                    }, function (error) {
                        deferPromise.reject(error);
                    });

                    return deferPromise.promise;
                };

                self.setSingle = function (newObj) {
                    var deferPromise = $q.defer();

                    var lamp = self.getSingle(newObj.id);

                    var dbObj = {};
                    for (var property in newObj) {
                        switch (property) {
                            case 'group':
                                var newProperty = "id_" + camelCase.toSeparate(property);
                                if (!newObj.hasOwnProperty(camelCase.fromString(newProperty))) {
                                    if (newObj[property] != null)
                                        dbObj[newProperty] = newObj[property].id;
                                    else
                                        dbObj[newProperty] = null;
                                    newObj[camelCase.fromString(newProperty)] = dbObj[newProperty];
                                }
                                break;
                            default:
                                dbObj[camelCase.toSeparate(property)] = newObj[property] == "null" ? null : newObj[property];
                        }
                    }

                    if (lamp !== null) {
                        // Found
                        if (newObj.idGroup == null || !newObj.active) {
                            // Not assigned then remove it
                            self.removeSingle(newObj.id).then(function () {
                                deferPromise.resolve();
                            }, function () {
                                deferPromise.reject();
                            });
                        } else {
                            // Assigned then update it
                            newObj.installed = selfObjArray[selfObjArray.indexOf(lamp)].installed;
                            selfObjArray[selfObjArray.indexOf(lamp)] = newObj;
                            DBService.DB_action(dbObj, factoryName, "UPDATE").then(function () {
                                deferPromise.resolve();
                            }, function () {
                                deferPromise.reject();
                            });
                        }
                    } else {
                        // Not found
                        var today = new Date();
                        today.setHours(0, 0, 0, 0);
                        if (newObj.idGroup != null && !!newObj.active && (!newObj.installed || newObj.rowVersion >= today.getTime())) {
                            // Assigned and not installed or recently updated, insert it
                            selfObjArray.push(newObj);
                            DBService.DB_action(dbObj, factoryName, "INSERT").then(function () {
                                deferPromise.resolve();
                            }, function () {
                                deferPromise.reject();
                            });
                        } else
                            deferPromise.resolve();
                    }

                    return deferPromise.promise;
                };

                self.removeSingle = function (id) {
                    var deferPromise = $q.defer();

                    var objToRemove = self.getSingle(id);
                    if (objToRemove != null) {
                        DBService.DB_action(objToRemove, factoryName, "DELETE").then(function () {
                            selfObjArray.splice(selfObjArray.indexOf(objToRemove), 1);
                            deferPromise.resolve();
                        }, function () {
                            deferPromise.reject();
                        });
                    }

                    return deferPromise.promise;
                };

                self.clear = function () {
                    // Erase all
                    var deferPromise = $q.defer();

                    selfObjArray = [];
                    DBService.DB_action([], factoryName, "DELETEALL").then(function () {
                        deferPromise.resolve();
                    }, function () {
                        deferPromise.reject();
                    });

                    return deferPromise.promise;
                };

                return self;
            }
        ]);