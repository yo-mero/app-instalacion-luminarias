var app = angular.module('app.common.service.session', []);

app.service('Session', ['$rootScope', '$q', '$log','DBService', function($rootScope, $q, $log, DBService) {
    var selfObjArray = [];
    var self = {};
    var factoryName = 'Session';

    self.init = function () {
        // Read from DB
        var deferPromise = $q.defer();
        DBService.DB_action([], factoryName, "GETALL").then(function (result) {
            if (result.rows.length > 0) {
                selfObjArray[0] = result.rows.item((result.rows.length - 1));
                deferPromise.resolve();
            }
            else {
                var newObj = {
                    id: 1,
                    username: "",
                    device_id_assigned: 0,
                    access_token: "",
                    token_type: "",
                    refresh_token: "",
                    app_version: "",
                    row_version: new Date(0).getTime()
                };

                selfObjArray.push(newObj);
                DBService.DB_action(newObj, factoryName, "INSERT").then(function () {
                    deferPromise.resolve();
                });
            }
        });

        return deferPromise.promise;
    };

    self.getAll = function () {
        // Get all
        return selfObjArray;
    };
    self.setAll = function (newObjArray) {
        // Set one by one
        for (var i = 0; i < newObjArray.length; i++) {
            self.setSingle(newObjArray[i]);
        }
    };
    self.clear = function () {
        // Erase all
        selfObjArray = [];
        return DBService.DB_action(newObj, factoryName, "DELETEALL");
    };
    self.getSingle = function (id) {
        // Get by id
        for (var i = 0; i < selfObjArray.length; i++) {
            if (selfObjArray[i].id == id) {
                return selfObjArray[i];
            }
        }
        return null;
    };
    self.setSingle = function (newObj) {
        // Set by id
        var session = self.getSingle(newObj.id);
        if (session !== null) {
            // Found, update it
            var index = selfObjArray.indexOf(session);
            selfObjArray[index] = newObj;
            return DBService.DB_action(newObj, factoryName, "UPDATE");
        }
        else {
            // Not found, insert it
            selfObjArray.push(newObj);
            return DBService.DB_action(newObj, factoryName, "INSERT");
        }
    };

    self.getUsername = function () {
        if (self.getSingle(1) !== null)
            return self.getSingle(1).username;
        return "";
    };
    self.setUsername = function (username) {
        if (selfObjArray[0].username != "" + username) {
            selfObjArray[0].username = username;
            $rootScope.Username = username;
            return DBService.DB_action(selfObjArray[0], factoryName, "UPDATE");
        }
    };

    self.getRowVersion = function () {
        if (self.getSingle(1) !== null)
            return Math.floor(self.getSingle(1).row_version);
        return 0;
    };
    self.setRowVersion = function (rowVersion) {
        var newDateRowVersion = new Date(rowVersion);
        if (rowVersion === 0 || newDateRowVersion.getTime() > selfObjArray[0].row_version) {
            $log.info("Actualizando row version a " + newDateRowVersion);
            selfObjArray[0].row_version = newDateRowVersion.getTime();
            return DBService.DB_action(selfObjArray[0], factoryName, "UPDATE");
        }
    };

    return self;

}]);