angular.module('app.common.db', [])
    .factory('DBService', ['$rootScope', '$q', '$ionicPlatform', '$log',
        function ($rootScope, $q, $ionicPlatform, $log) {

            var self = {};
            var session_create_query = 'CREATE TABLE IF NOT EXISTS user_data(id integer primary key, device_id_assigned integer, username text(50), access_token char(40), token_type char(20), refresh_token char(40), app_version text, row_version text)';
            var session_insert_query = 'INSERT INTO user_data(id, device_id_assigned, username, access_token, token_type, refresh_token, app_version, row_version) VALUES(?,?,?,?,?,?,?,?)';
            var session_getall_query = 'SELECT * FROM user_data WHERE 1 ORDER BY id';
            var session_update_query = 'UPDATE user_data SET device_id_assigned = ?, username=?, access_token=?, token_type=?, refresh_token=?, app_version=?, row_version=? WHERE id=?';
            var session_deleteall_query = 'DELETE FROM user_data WHERE true';

            var positions_create_query = 'CREATE TABLE IF NOT EXISTS positions (id integer key unique, name text, latitude real, longitude real, circuit text, watts text, number_of_lamps integer, linked_with integer, installed integer default 0, active integer default 1, id_group integer, row_version text)';
            var positions_insert_query = 'INSERT INTO positions (id, name, latitude, longitude, circuit, watts, number_of_lamps, linked_with, installed, active, id_group, row_version) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)';
            var positions_update_query = 'UPDATE positions SET name=?, latitude=?, longitude=?, circuit=?, watts=?, number_of_lamps=?, linked_with=?, installed=?, active=?, id_group=?, row_version=? WHERE id=?';
            var positions_delete_query = 'DELETE FROM positions WHERE id=?';
            var positions_getall_query = 'SELECT * FROM positions WHERE 1 ORDER BY id';
            var positions_deleteall_query = 'DELETE FROM positions WHERE 1';
            var positions_deleteold_query = 'DELETE FROM positions WHERE (installed=0 OR installed="false") AND row_version<?';

            var lamps_create_query = 'CREATE TABLE IF NOT EXISTS lamps (id integer key unique, serial text, info text, type text, watts text, installed integer default 0, active integer default 1, id_group integer, row_version text)';
            var lamps_insert_query = 'INSERT INTO lamps (id, serial, info, type, watts, installed, active, id_group, row_version) VALUES (?,?,?,?,?,?,?,?,?)';
            var lamps_update_query = 'UPDATE lamps SET serial=?, info=?, type=?, watts=?, installed=?, active=?, id_group=?, row_version=? WHERE id=?';
            var lamps_delete_query = 'DELETE FROM lamps WHERE id=?';
            var lamps_getall_query = 'SELECT * FROM lamps WHERE 1 ORDER BY id';
            var lamps_deleteall_query = 'DELETE FROM lamps WHERE 1';
            var lamps_deleteold_query = 'DELETE FROM lamps WHERE (installed=0 OR installed="false") AND row_version<?';

            var installation_create_query = 'CREATE TABLE IF NOT EXISTS installation (id integer primary key, status varchar(10) default \'PENDIENTE\', id_group integer, id_position integer, id_lamp integer, installed_date text, lamp_exists integer default 1, photo_before mediumtext, photo_after mediumtext, removed_type text, removed_power text, removed_bal text, address text null, address_number text null, street1 text null, street2 text null, address_extra text null, comments text null, gps_coords text not null, linked integer default 0, sync integer default 0, row_version text)';
            var installation_insert_query = 'INSERT INTO installation (id, status, id_group, id_position, id_lamp, installed_date, lamp_exists, photo_before, photo_after, removed_type, removed_power, removed_bal, address, address_number, street1, street2, address_extra, comments, gps_coords, linked, sync, row_version) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
            var installation_update_query = 'UPDATE installation SET status=?, id_position=?, id_lamp=?, installed_date=?, lamp_exists=?, photo_before=?, photo_after=?, removed_type=?, removed_power=?, removed_bal=?, address=?, address_number=?, street1=?, street2=?, address_extra=?, comments=?, gps_coords=?, linked=?, sync=?, row_version=? WHERE id=? AND id_group=?';
            var installation_changeid_query = 'UPDATE installation SET id=? WHERE id=?';
            var installation_installed_query = 'UPDATE installation SET status=\'GUARDADO\', photo_before="", photo_after="", sync=1 WHERE id=?';
            var installation_delete_query = 'DELETE FROM installation WHERE id=?';
            var installation_getall_query = 'SELECT * FROM installation WHERE 1  ORDER BY id';
            var installation_getbyid_query = 'SELECT * FROM installation WHERE id=?';
            var installation_deleteall_query = 'DELETE FROM installation WHERE 1';

            var groups_create_query = 'CREATE TABLE IF NOT EXISTS groups (id integer key unique, name text, alm_clave integer, cd_clave integer, edo_clave integer, pais_clave integer, active integer default 1, row_version text)';
            var groups_insert_query = 'INSERT INTO groups (id, name, alm_clave, cd_clave, edo_clave, pais_clave, active, row_version) VALUES (?,?,?,?,?,?,?,?)';
            var groups_update_query = 'UPDATE groups SET name=?, alm_clave=?, cd_clave=?, edo_clave=?, pais_clave=?, active=?, row_version=? WHERE id=?';
            var groups_delete_query = 'DELETE FROM groups WHERE id=?';
            var groups_getall_query = 'SELECT * FROM groups WHERE 1 ORDER BY id';
            var groups_getbyid_query = 'SELECT * FROM groups WHERE id=?';
            var groups_deleteall_query = 'DELETE FROM groups WHERE 1';

            var issues_create_query = 'CREATE TABLE IF NOT EXISTS issues (id integer key unique, module text, value text, active integer, row_version text)';
            var issues_insert_query = 'INSERT INTO issues (id, module, value, active, row_version) VALUES (?,?,?,?,?)';
            var issues_update_query = 'UPDATE issues SET module=?, value=?, active=?, row_version=? WHERE id=?';
            var issues_delete_query = 'DELETE FROM issues WHERE id=?';
            var issues_getall_query = 'SELECT * FROM issues WHERE 1 ORDER BY id';
            var issues_deleteall_query = 'DELETE FROM issues WHERE 1';

            var locations_create_query = 'CREATE TABLE IF NOT EXISTS locations (date text, idGroup integer, coords text)';
            var locations_insert_query = 'INSERT INTO locations (date, idGroup, coords) VALUES (?, ?, ?)';
            var locations_getall_query = 'SELECT * FROM locations WHERE 1 ORDER BY date';
            var locations_delete_query = 'DELETE FROM locations WHERE date=?';
            var locations_deleteall_query = 'DELETE FROM locations WHERE 1';

            self.createTablesQuerys = [
                session_create_query,
                positions_create_query,
                lamps_create_query,
                installation_create_query,
                groups_create_query,
                issues_create_query,
                locations_create_query,
            ];
            self.appUpdates = function (currentAppVersion, newAppVersion) {
                var deferPromise = $q.defer();

                if (!!currentAppVersion && currentAppVersion != newAppVersion && newAppVersion != "emulate") {
                    var appVersionParts = currentAppVersion.split('.').map(Number);
                    var updatesQuerys = [];
                    var updatesQuerysPerVersion = {
                        // Major
                        "3": {
                            // Minor
                            "2": {
                                // Maintenance
                                "2": [
                                    {
                                        "canIgnore": false,
                                        "query": [
                                            "ALTER TABLE installation RENAME TO backup_installation",
                                            installation_create_query,
                                            "INSERT INTO installation (id, status, id_group, id_position, id_lamp, installed_date, photo_before, photo_after, removed_type, removed_power, removed_bal, address, address_number, street1, street2, comments, gps_coords, linked, sync, row_version) " +
                                            "SELECT id, status, id_group, id_position, id_lamp, installed_date, photo_before, photo_after, removed_type, removed_power, removed_bal, address, address_number, street1, street2, comments, gps_coords, linked, sync, row_version FROM backup_installation",
                                            "DROP TABLE backup_installation"
                                        ]
                                    }
                                ]
                            },
                            "3": {
                                "1": [
                                    {
                                        "canIgnore": false,
                                        "query": [
                                            "ALTER TABLE installation RENAME TO backup_installation",
                                            installation_create_query,
                                            "INSERT INTO installation (id, status, id_group, id_position, id_lamp, installed_date, lamp_exists, photo_before, photo_after, removed_type, removed_power, removed_bal, address, address_number, street1, street2, comments, gps_coords, linked, sync, row_version) " +
                                            "SELECT id, status, id_group, id_position, id_lamp, installed_date, lamp_exists, photo_before, photo_after, removed_type, removed_power, removed_bal, address, address_number, street1, street2, comments, gps_coords, linked, sync, row_version FROM backup_installation",
                                            "DROP TABLE backup_installation"
                                        ]
                                    }
                                ]
                            }
                        }
                    };

                    function checkVersionLevel(objectQueryPerVersion, indexAppVersion, previousGreater) {
                        if (!objectQueryPerVersion.length) {
                            if (!previousGreater) {
                                for (var key in objectQueryPerVersion) {
                                    if (appVersionParts[indexAppVersion] == undefined || Number(key) > appVersionParts[indexAppVersion])
                                        checkVersionLevel(objectQueryPerVersion[key], indexAppVersion + 1, true);
                                    else if (Number(key) == appVersionParts[indexAppVersion])
                                        checkVersionLevel(objectQueryPerVersion[key], indexAppVersion + 1, false);
                                }
                            } else {
                                for (var key in objectQueryPerVersion)
                                    checkVersionLevel(objectQueryPerVersion[key], indexAppVersion + 1, previousGreater);
                            }
                        } else {
                            if (!!previousGreater) {
                                angular.forEach(objectQueryPerVersion, function (listQuerys) {
                                    if (!listQuerys.canIgnore)
                                        updatesQuerys = updatesQuerys.concat(listQuerys.query);
                                });
                            }
                        }
                    }

                    for (var key in updatesQuerysPerVersion)
                        checkVersionLevel(updatesQuerysPerVersion[key], 0, false);

                    var updatesOK = true;
                    angular.forEach(updatesQuerys, function (query) {
                        self.query(query).then(function () {
                            $log.info("Success table update");
                        }, function (error) {
                            $log.error("Error on table update " + angular.toJson(error));
                            deferPromise.reject(error);
                            updatesOK = false;
                            return false;
                        });
                    });
                    if (updatesOK)
                        deferPromise.resolve(newAppVersion);
                } else {
                    deferPromise.resolve(newAppVersion);
                }

                return deferPromise.promise;
            };

            self.createTables = function () {
                var q = $q.defer();
                try {
                    angular.forEach(self.createTablesQuerys, function (query) {
                        self.query(query).then(function () {
                            $log.info('Success table creation');
                            q.resolve(true);
                        }, function (error) {
                            $log.error("Error on create table " + angular.toJson(error));
                            q.reject(error);
                        });
                    });
                } catch (e) {
                    q.reject(e);
                }
                return q.promise;
            };
            // Process an action at DB layer
            self.DB_action = function (rxObj, service, action) {
                return eval("self.process" + service + "(rxObj, action)");
            };

            self.processSession = function (rxObj, action) {
                switch (action) {
                    case "INSERT":
                        parameters = [rxObj.id, parseInt(rxObj.device_id_assigned) || 0, rxObj.username, rxObj.access_token, rxObj.token_type, rxObj.refresh_token, rxObj.app_version, "" + Math.floor(rxObj.row_version)];
                        break;
                    case "UPDATE":
                        parameters = [parseInt(rxObj.device_id_assigned) || 0, rxObj.username, rxObj.access_token, rxObj.token_type, rxObj.refresh_token, rxObj.app_version, "" + Math.floor(rxObj.row_version), rxObj.id];
                        break;
                    default:
                        parameters = rxObj;
                }

                var query = eval("session_" + action.toLowerCase() + "_query");

                return self.query(query, parameters);
            };

            self.processInstallation = function (rxObj, action) {
                switch (action) {
                    case "INSERT":
                        parameters = [rxObj.id, rxObj.status, rxObj.id_group, rxObj.id_position, rxObj.id_lamp, "" + Math.floor(rxObj.installed_date), rxObj.lamp_exists, rxObj.photo_before, rxObj.photo_after, rxObj.removed_type, rxObj.removed_power, rxObj.removed_bal, rxObj.address, rxObj.address_number, rxObj.street1, rxObj.street2, rxObj.address_extra, rxObj.comments, rxObj.gps_coords, rxObj.linked, rxObj.sync, "" + Math.floor(rxObj.row_version)];
                        break;
                    case "UPDATE":
                        parameters = [rxObj.status, rxObj.id_position, rxObj.id_lamp, "" + Math.floor(rxObj.installed_date), rxObj.lamp_exists, rxObj.photo_before, rxObj.photo_after, rxObj.removed_type, rxObj.removed_power, rxObj.removed_bal, rxObj.address, rxObj.address_number, rxObj.street1, rxObj.street2, rxObj.address_extra, rxObj.comments, rxObj.gps_coords, rxObj.linked, rxObj.sync, "" + Math.floor(rxObj.row_version), rxObj.id, rxObj.id_group];
                        break;
                    case "GETBYID":
                    case "INSTALLED":
                    case "DELETE":
                        parameters = [rxObj.id];
                        break;
                    default:
                        parameters = rxObj;
                }

                var query = eval("installation_" + action.toLowerCase() + "_query");

                return self.query(query, parameters);
            };

            self.processPositions = function (rxObj, action) {
                switch (action) {
                    case "INSERT":
                        parameters = [rxObj.id, rxObj.name, rxObj.latitude, rxObj.longitude, rxObj.circuit, rxObj.watts, rxObj.number_of_lamps, rxObj.linked_with, +rxObj.installed, +rxObj.active, rxObj.id_group, "" + Math.floor(rxObj.row_version)];
                        break;
                    case "UPDATE":
                        parameters = [rxObj.name, rxObj.latitude, rxObj.longitude, rxObj.circuit, rxObj.watts, rxObj.number_of_lamps, rxObj.linked_with, +rxObj.installed, +rxObj.active, rxObj.id_group, "" + Math.floor(rxObj.row_version), rxObj.id];
                        break;
                    case "DELETE":
                        parameters = [rxObj.id];
                        break;
                    default:
                        parameters = rxObj;
                }

                var query = eval("positions_" + action.toLowerCase() + "_query");

                return self.query(query, parameters);
            };

            self.processLamps = function (rxObj, action) {
                switch (action) {
                    case "INSERT":
                        parameters = [rxObj.id, rxObj.serial, rxObj.info, rxObj.type, rxObj.watts, +rxObj.installed, +rxObj.active, rxObj.id_group, "" + Math.floor(rxObj.row_version)];
                        break;
                    case "UPDATE":
                        parameters = [rxObj.serial, rxObj.info, rxObj.type, rxObj.watts, +rxObj.installed, +rxObj.active, rxObj.id_group, "" + Math.floor(rxObj.row_version), rxObj.id];
                        break;
                    case "DELETE":
                        parameters = [rxObj.id];
                        break;
                    default:
                        parameters = rxObj;
                }

                var query = eval("lamps_" + action.toLowerCase() + "_query");

                return self.query(query, parameters);
            };

            self.processGroups = function (rxObj, action) {
                switch (action) {
                    case "INSERT":
                        parameters = [rxObj.id, rxObj.name, rxObj.alm_clave, rxObj.cd_clave, rxObj.edo_clave, rxObj.pais_clave, +rxObj.active, "" + Math.floor(rxObj.row_version)];
                        break;
                    case "UPDATE":
                        parameters = [rxObj.name, rxObj.alm_clave, rxObj.cd_clave, rxObj.edo_clave, rxObj.pais_clave, +rxObj.active, "" + Math.floor(rxObj.row_version), rxObj.id];
                        break;
                    case "GETBYID":
                    case "DELETE":
                        parameters = [rxObj.id];
                        break;
                    default:
                        parameters = rxObj;
                }

                var query = eval("groups_" + action.toLowerCase() + "_query");

                return self.query(query, parameters);
            };

            self.processIssues = function (rxObj, action) {
                switch (action) {
                    case "INSERT":
                        parameters = [rxObj.id, rxObj.module, rxObj.value, rxObj.active, "" + Math.floor(rxObj.row_version)];
                        break;
                    case "UPDATE":
                        parameters = [rxObj.module, rxObj.value, rxObj.active, "" + Math.floor(rxObj.row_version), rxObj.id];
                        break;
                    case "DELETE":
                        parameters = [rxObj.id];
                        break;
                    default:
                        parameters = rxObj;
                }

                var query = eval("issues_" + action.toLowerCase() + "_query");

                return self.query(query, parameters);
            };

            self.processLocations = function (rxObj, action) {
                switch (action) {
                    case "INSERT":
                        parameters = ["" + Math.floor(rxObj.date), rxObj.idGroup, rxObj.coords];
                        break;
                    case "DELETE":
                        parameters = ["" + Math.floor(rxObj.date)];
                        break;
                    default:
                        parameters = rxObj;
                }

                var query = eval("locations_" + action.toLowerCase() + "_query");

                return self.query(query, parameters);
            };

            //method to make querys, insert, update, delete or select
            self.query = function (query, parameters) {
                parameters = parameters || [];
                var q = $q.defer();
                try {
                    $rootScope.db.transaction(function (tx) {
                        //console.debug("Executing " + query);
                        tx.executeSql(query, parameters, function (tx, res) {
                            //console.debug("Success " + angular.toJson(res));
                            q.resolve(res);
                        }, function (transaction, e) {
                            $log.error('ERROR DAO Error: ' + e.message);
                            q.reject(e);
                        });
                    });
                } catch (e) {
                    $log.error("Error al acceder a la Base de Datos: " + e);
                    q.reject(e);
                }

                return q.promise;
            };

            //process result set
            self.getResult = function (result) {
                var res = [];
                for (var i = 0; i < result.rows.length; i++) {
                    res.push(result.rows.item(i));
                }
                return res;
            };

            //process single result
            self.uniqueResult = function (result) {
                var unique = null;
                //console.debug("Resultado: " + result.rows.length);
                if (result.rows.length > 0) {
                    unique = result.rows.item(0);
                }

                return unique;
            };

            return self;

        }
    ]);