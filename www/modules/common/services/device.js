angular.module('app.common.service.device', [])
    .service('Device', ['$cordovaDevice', 'Session', 'ENVIRONMENT',
        function ($cordovaDevice, Session, ENVIRONMENT) {
            return {
                Info: function () {
                    var deviceInfoObj = {};
                    if (window.cordova)
                        deviceInfoObj = $cordovaDevice.getDevice();
                    else if (ENVIRONMENT !== 'P' && ENVIRONMENT !== 'D') {
                        deviceInfoObj = {
                            manufacturer: "Development",
                            model: "",
                            serial: ""
                        };
                    }

                    return deviceInfoObj;
                },
                InfoForRequest: function () {
                    var device = this.Info();
                    var deviceString = '';
                    if (window.cordova)
                        deviceString = "device=" + device.manufacturer + " " + device.model + " " + device.serial + "&uuid=" + device.uuid;
                    else if (ENVIRONMENT !== 'P' && ENVIRONMENT !== 'D') {
                        deviceString = "device=Development&uuid=00000";
                    }

                    return deviceString;
                },
                VersionRunning: function () {
                    return Session.getAll().slice(0).pop().app_version;
                }
            };
        }
    ]);