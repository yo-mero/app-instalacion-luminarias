/* jslint    */
/*global app */
'use strict';

  app.filter('capitalize', function() {
    return function(input) {
      if (input!==null){
        input = input.toLowerCase();
      }
      return input.substring(0,1).toUpperCase()+input.substring(1);
    };
  })

  .filter('trim_capitalize', function() {
    return function(input) {
      input = input.toLowerCase();
      input = input.substring(0,1).toUpperCase()+input.substring(1);
      input = input.replace(" ", "");
      return input;
    };
  })

  .filter('MMMes', ['$filter', function ($filter) {
      return function (htmlData) {
          if (htmlData !== undefined) {
              var meses = ['','Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
              return meses[$filter('date')(htmlData,'M')];
          }
          return '';
      };
  }])

  .filter('translate', function () {
      return function (htmlData) {
          if (htmlData !== undefined) {
              var translates = {
                  NEW:"NUEVO",
                  DELIVER:"ENTREGADO",
                  CANCEL:"CANCELADO"
              };
              return translates[htmlData];
          }
          return '';
      };
  })

   .filter('loop', function() {
      return function(input) {
        var start, end, step;
        switch (input.length) {
          case 1:
            start = 0;
            end = parseInt(input[0]);
            step = 1;
            break;
          case 2:
            start = parseInt(input[0]);
            end = parseInt(input[1]);
            step = 1;
            break;
          case 3:
            start = parseInt(input[0]);
            end = parseInt(input[1]);
            step = parseInt(input[2]);
            break;
          default:
            return input;
        }
        var arrayLoop = [];
        for (var i = start; i < end; i+=step) {
          arrayLoop.push(i);
        }
        return arrayLoop;
      };
   });
