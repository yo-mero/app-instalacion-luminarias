/**
 * Created by gcuellar on 10/11/17.
 */
angular.module('app').directive('formElement', ['$compile', '$interval', function ($compile, $interval) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            options: '=',
            model: '='
        },
        link: function (scope, element, attributes) {
            var formEl;
            switch (attributes.type) {
                case 'switch':
                    var switchText = {
                        "false": "No",
                        "true": "Si"
                    };
                    if (scope.options !== undefined) {
                        if (scope.options !== null) {
                            if (typeof scope.options === 'object') {
                                if (scope.options.hasOwnProperty('false') && scope.options.hasOwnProperty('true'))
                                    switchText = scope.options;
                                else {
                                    var keys = Object.keys(switchText);
                                    for (var i = 0; i < keys.length && i < scope.options.length; i++)
                                        switchText[keys[i]] = scope.options[i];
                                }
                            }
                        } else
                            switchText = {
                                "false": '',
                                "true": ''
                            };
                    }

                    formEl = angular.element('<label>').addClass('input-' + attributes.type);
                    formEl.append(angular.element('<span>').addClass('option').html(switchText.false));
                    formEl.append(angular.element('<input type="checkbox" ng-model="model">'));
                    formEl.append(angular.element('<span>').addClass('slider round'));
                    formEl.append(angular.element('<span>').addClass('option').html(switchText.true));
                    break;
                case 'number':
                    scope.numberOptions = {
                        step: 1,
                        min: 0,
                        max: -1
                    };
                    if (!!scope.options) {
                        scope.numberOptions.step = scope.options.step || scope.numberOptions.step;
                        scope.numberOptions.min = scope.options.min || scope.numberOptions.min;
                        scope.numberOptions.max = scope.options.max || scope.numberOptions.max;
                    }
                    if (scope.numberOptions.max > 0 && scope.model > scope.numberOptions.max)
                        scope.model = scope.numberOptions.max;
                    if (scope.model < scope.numberOptions.min)
                        scope.model = scope.numberOptions.min;

                    var promise = null;
                    scope.numberUp = function (hold) {
                        if (promise)
                            $interval.cancel(promise);
                        if (scope.numberOptions.max < 0 || scope.model < scope.numberOptions.max) {
                            // can't use a simply model + step because javascript have issues with precision in real numbers
                            if (!hold)
                                scope.model = Math.round((scope.model + scope.numberOptions.step) * 1e12) / 1e12;
                            else
                                promise = $interval(function () {
                                    if (scope.numberOptions.max < 0 || scope.model < scope.numberOptions.max)
                                        scope.model = Math.round((scope.model + scope.numberOptions.step) * 1e12) / 1e12;
                                }, 50);
                        }
                    };
                    scope.numberDown = function (hold) {
                        if (promise)
                            $interval.cancel(promise);
                        if (scope.model > scope.numberOptions.min) {
                            // can't use a simply model - step because javascript have issues with precision in real numbers
                            if (!hold)
                                scope.model = Math.round((scope.model - scope.numberOptions.step) * 1e12) / 1e12;
                            else
                                promise = $interval(function () {
                                    if (scope.model > scope.numberOptions.min)
                                        scope.model = Math.round((scope.model - scope.numberOptions.step) * 1e12) / 1e12;
                                }, 50);
                        }
                    }
                    scope.cancelNumber = function () {
                        if (promise)
                            $interval.cancel(promise);
                        promise = null;
                    };

                    formEl = angular.element('<span>').addClass('input-' + attributes.type);
                    formEl.append(angular.element('<span on-touch="numberDown()" on-hold="numberDown(true)" on-release="cancelNumber()" ng-class="model==numberOptions.min?\'disabled\':\'\'">').addClass('option ion-android-remove-circle'));
                    formEl.append(angular.element('<input type="number" ng-model="model" step="{{numberOptions.step}}" min="{{numberOptions.min}}" max="{{numberOptions.max>=0?numberOptions.max:\'\'}}">').addClass('input-value'));
                    formEl.append(angular.element('<span on-touch="numberUp()" on-hold="numberUp(true)" on-release="cancelNumber()" ng-class="model==numberOptions.max?\'disabled\':\'\'">').addClass('option ion-android-add-circle'));
                    break;
                case 'select':
                    scope.itemsForSelect = [];
                    if (!!scope.options.items.length && typeof scope.options.items === 'object') {
                        for (var i = 0; i < scope.options.items.length; i++) {
                            if (typeof scope.options.items[i] === 'string') {
                                scope.itemsForSelect.push({value: scope.options.items[i], tag: scope.options.items[i]});
                            } else
                                scope.itemsForSelect.push(scope.options.items[i]);
                        }
                    }

                    formEl = angular.element('<span>').addClass('input-' + attributes.type);
                    if (!!scope.options.default)
                        var defaultOpt = angular.element('<option value="">').html((scope.options.default.txt || 'Seleccione...')).attr('disabled', !scope.options.default.pickable);
                    else
                        var defaultOpt = angular.element('<option value="" disabled>').html('Seleccione...');
                    formEl.append(angular.element('<select ng-model="model" ng-options="item.value as item.tag for item in itemsForSelect">').append(defaultOpt));
                    break;
            }
            $compile(formEl)(scope);
            element.replaceWith(formEl)
        }
    }
}]);