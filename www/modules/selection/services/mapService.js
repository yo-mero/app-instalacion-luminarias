angular.module('app.installation.map', [])
.service('Map', ['$rootScope', '$http', '$ionicPopup', 'GPS', function($rootScope, $http, $ionicPopup, GPS) {
    var self = {}, markers = [], MarkerSize = 10, map = null, littleMap = null, infoWindow, savedLocation;

    self.init = function (positions) {
        if (!!positions) {
            GPS.init();

            if (typeof google !== 'undefined') {
                if (!map) {
                    map = new google.maps.Map(document.getElementById('map'), {
                        center: {
                            lat: $rootScope.gps.latitude,
                            lng: $rootScope.gps.longitude
                        },
                        gestureHandling: 'cooperative',
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        styles: [
                            {
                                "featureType": "poi",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "landscape.natural",
                                "elementType": "geometry",
                                "stylers": [
                                    {"color": "#cbe6a3"}
                                ]
                            }
                        ],
                        disableDefaultUI: true,
                        zoom: 17,
                        zoomControl: true
                    });

                    self.setCenterControl(map);
                    new GeolocationMarker(map);
                } else {
                    document.getElementById('map').parentNode.replaceChild(map.getDiv(), document.getElementById('map'));
                    map.setZoom(17);
                    map.setCenter({lat: $rootScope.gps.latitude, lng: $rootScope.gps.longitude});
                }

                if (!infoWindow)
                    infoWindow = new google.maps.InfoWindow();

                self.setMarkers(positions);

                if (markers.length) {
                    var boundMarkersCoords = new google.maps.LatLngBounds(); // Save Bounds google maps LatLngBounds obj to center map in markers
                    for (var i = 0; i < markers.length; i++) {
                        boundMarkersCoords.extend(markers[i].getPosition());
                    }
                    map.fitBounds(boundMarkersCoords);
                    var listener = google.maps.event.addListener(map, "idle", function() {
                        if (map.getZoom() > 17)
                            map.setZoom(17);
                        google.maps.event.removeListener(listener);
                    });
                }
            }

            return map;
        }
        else {
            $ionicPopup.alert({
                title: '<b>Error</b>',
                template: 'No se ha enviado la información de los postes para este movimiento.'
            });
            document.location.href = 'index.html';
        }
    };

    self.initLittleMap = function () {
        if (typeof google !== 'undefined') {
            self.clearMarkers();

            function startMap() {
                var element = document.getElementById('littleMap');
                element.id = 'map';
                littleMap = new google.maps.Map(element, {
                    center: {
                        lat: savedLocation.latitude,
                        lng: savedLocation.longitude
                    },
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    styles: [
                        {
                            "featureType": "poi",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape.natural",
                            "elementType": "geometry",
                            "stylers": [
                                {"color": "#cbe6a3"}
                            ]
                        }
                    ],
                    disableDefaultUI: true,
                    zoom: 16,
                    zoomControl: true
                });

                self.setCenterControl(littleMap);
                new GeolocationMarker(littleMap);

                return littleMap;
            }
            setTimeout(startMap, 1000);
        }
    };

    self.setCenterControl = function (mapToSet) {
        var centerControlDiv = document.createElement('div');
        new self.CenterControl(centerControlDiv, mapToSet);
        centerControlDiv.index = 1;
        mapToSet.controls[google.maps.ControlPosition.TOP_RIGHT].push(centerControlDiv);
    };

    self.setMarkers = function (positions) {
        self.clearMarkers();
        for (var i = 0; i < positions.length; i++) {
            var position = positions[i];
            if (position.latitude !== "" && position.longitude !== "") {
                if (position.installed)
                    var iconURL = 'img/map-marker-installed.png';
                else
                    var iconURL = 'img/map-marker.png';
                var marker = new google.maps.Marker({
                    map: map,
                    icon: new google.maps.MarkerImage(iconURL,
                        new google.maps.Size(MarkerSize, MarkerSize),
                        new google.maps.Point(0, 0),
                        new google.maps.Point(0, 0),
                        new google.maps.Size(MarkerSize, MarkerSize)
                    ),
                    position: new google.maps.LatLng(parseFloat(position.latitude), parseFloat(position.longitude)),
                    positionIndex: i,
                    infoWindow: 'Posición: ' + position.name + ' ' + position.watts + 'W'
                });
                markers.push(marker);
            }
        }

        google.maps.event.addListener(map, 'zoom_changed', function() {
            var largeur = MarkerSize + (2 *(map.getZoom() - 14));
            if (largeur < MarkerSize)
                largeur = MarkerSize;
            var hauteur = largeur;

            for(var i=0; i < markers.length; i++) {
                var icon = markers[i].getIcon();
                markers[i].setIcon(new google.maps.MarkerImage(
                    icon.url,
                    new google.maps.Size(largeur, hauteur),
                    new google.maps.Point(0, 0),
                    new google.maps.Point(largeur/2, hauteur/2),
                    new google.maps.Size(largeur, hauteur))
                );
            }
        });
    };

    self.clearMarkers = function () {
        if (markers.length > 0) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }
            markers.length = 0;
        }
    };

    self.getMarkers = function () {
        return markers;
    };

    self.showInfoWindow = function (marker) {
        infoWindow.setContent(marker.infoWindow);
        infoWindow.open(marker.map, marker);
    };

    self.CenterControl = function (controlDiv, map) {
        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.id = 'mapCenterControl';
        controlUI.title = 'Click to recenter the map';

        var controlIcon = document.createElement('div');
        controlIcon.className = "center-icon";
        controlUI.appendChild(controlIcon);

        controlDiv.appendChild(controlUI);

        controlUI.addEventListener('click', function() {
            map.panTo({lat: $rootScope.gps.latitude, lng: $rootScope.gps.longitude});
            if (map.getZoom() < 17)
                map.setZoom(17);
        });
    };

    self.updateDistance = function (position) {
        position.distance = GPS.calculateDistance($rootScope.gps,position);
        position.distanceMeters = parseInt(GPS.calculateDistance($rootScope.gps,position,true));
    };

    self.saveLocation = function (coordsObj) {
        savedLocation = coordsObj || {latitude: $rootScope.gps.latitude, longitude: $rootScope.gps.longitude};
    };

    self.currentPosition = function () {
        return savedLocation;
    };

    self.getGeocoding = function () {
        if (!!savedLocation)
            return $http.get('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyAEmSkqgDu-GafUthP0zN8wON3JdS5AIvM&latlng=' + $rootScope.gps.latitude + ',' + $rootScope.gps.longitude)
                .then(function (data) {
                    return data.data.results[0];
                });
        return false;
    };

    return self;
}]);