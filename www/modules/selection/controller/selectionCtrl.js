app.controller('selectionController',
['$scope', '$rootScope', '$state', '$q', '$ionicModal', '$ionicPopup', '$cordovaCamera', '$ionicScrollDelegate', '$ionicPosition', 'Map', 'Group', 'Positions', 'Lamps', 'Install', 'AppServices', 'localStorage', 'ENVIRONMENT',
    function ($scope, $rootScope, $state, $q, $ionicModal, $ionicPopup, $cordovaCamera, $ionicScrollDelegate, $ionicPosition, Map, Group, Positions, Lamps, Install, AppServices, localStorage, ENVIRONMENT) {
        $rootScope.isFormData = false; // Control variable to disable/enable the back button on devices
        // Object to save and show data in view
        var cleanDataObject = {
            group: '',
            position: [],
            lamp: [],
            lampExists: [],
            photoBefore: '',
            photoAfter: '',
            finished: false,
            comments: '',
            removedType: [],
            removedPower: [],
            removedBal: [],
            address: '',
            addressNumber: '',
            street1: '',
            street2: '',
            savesubmited: false
        };
        var cleanDataControlObject = {
            positionList: undefined, // Save the whole list of positions (position)
            lampsList: undefined, // Save the whole list of lamps
            indexSelectedGroup: undefined, // Save the index in array of the selected group
            indexPositionToSelect: undefined, // Save the index of the position (position) to select (with which button the map was activated)
            indexSelectedPosition: [], // Save the index in array of the selected position
            indexSelectedLamp: [], // Save the index in array of the selected lamp
            installedID: [], // Save the list IDs of installations, assigned/updated when an installed position (position) is selected
            indexInstalledPosition: [], // Index list of position installed
            indexInstalledLamps: [], // Index list of lamps installed
            allLampsSelected: false // Boolean to validate if all the lamps are selected
        };
        var cleanControlObject = {
            buttonDisabled: true, // Boolean to add the disabled class to the button when data isn't complete
            editingInstallForPhotos: false, // Boolean to control if is an edition and photos don't need to be replaced
            numOfLampsInPosition: 1, // Number of lamps in the selected post
            allPhotosTaked: false // Boolean to validate if all the photos (before & after) was taken before the last phase
        };
        $scope.data = {};
        var dataControl = {};
        $scope.control = {};

        function resetData() {
            $rootScope.isFormData = false;
            angular.copy(cleanDataObject, $scope.data);
            angular.copy(cleanDataControlObject, dataControl);
            angular.copy(cleanControlObject, $scope.control);

            $scope.data.finished = false;
            $scope.data.savesubmited = false;

            if ($state.params.idGroup > 0) {
                var group = Group.getSingle($state.params.idGroup);
                if (group != null) {
                    $scope.data.group = group.name;
                    groupSelectedTrigger();

                    function groupSelectedTrigger() {
                        if ($scope.groupSelected === undefined)
                            setTimeout(groupSelectedTrigger, 500);
                        else
                            $scope.groupSelected(true);
                    }
                }
            }
        }
        resetData(); // Initialize

        /** Group **/
        // Get groups
        $scope.groups = Group.getAll();

        // Function to look for the group with the name
        var GroupByName = function (group) {
            return group.name == this;
        };
        // When a group is selected
        $scope.groupSelected = function (fromSession) {
            var syncPromise = [];
            fromSession = !!fromSession;
            if (!fromSession) {
                var group = $scope.groups.find(GroupByName, $scope.data.group);
                $state.go($state.current, {idGroup: group.id}, {reload: false});
            } else
                syncPromise.push(AppServices.updateData($state.params.idGroup));

            $q.all(syncPromise).then(function () {
                // Get the group index of the group selected
                dataControl.indexSelectedGroup = $scope.groups.findIndex(GroupByName, $scope.data.group);
                // Get position actives in group
                dataControl.positionList = Positions.getByGroup($scope.groups[dataControl.indexSelectedGroup], Positions.getActives());
                if (ENVIRONMENT === 'P' || ENVIRONMENT === 'D')
                    dataControl.positionList = dataControl.positionList.filter(function (position) {
                        return position.id > 0;
                    });
                // Get lamps in group
                dataControl.lampsList = Lamps.getByFilter($scope.groups[dataControl.indexSelectedGroup], Install.getAll().map(function (obj) {
                    return +obj.idLamp;
                }));
            }, function (response) {
                if (response.confirm) // Retry
                    $scope.groupSelected(fromSession);
                else if (!response.hasData) // Cancel/Close
                    ionic.Platform.exitApp();
            });
        };
        /** ~ **/

        /** Position **/
        // When a position is selected, check if is a different that the current selected
        var listWattsInPositions = [];
        var changePosition = function (position, installation) {
            if (typeof dataControl.indexPositionToSelect != 'undefined' && dataControl.indexPositionToSelect >= 0) {
                if (position.name != $scope.data.position[dataControl.indexPositionToSelect]) {
                    listWattsInPositions = [];
                    if (dataControl.indexPositionToSelect == 0) {
                        var backupGroup = $scope.data.group;
                        angular.copy(cleanDataObject, $scope.data);
                        $scope.data.group = backupGroup;
                        dataControl.indexSelectedPosition = [];
                        dataControl.indexSelectedLamp = [];
                        dataControl.indexInstalledPosition = [];
                        dataControl.indexInstalledLamps = [];

                        dataControl.indexSelectedPosition.push(dataControl.positionList.indexOf(position));
                        $scope.data.position.push(position.name);
                        $scope.data.lampExists.push(true);
                        listWattsInPositions.push(position.watts);
                        $scope.control.numOfLampsInPosition = position.numberOfLamps;

                        if (position.numberOfLamps > 1) {
                            installation = installation || null;
                            var linkedPosition = [];
                            if (installation !== null) {
                                var linkedInstallations = Install.getLinkedInstallations(installation);
                                for (i = 0; i < linkedInstallations.length; i++) {
                                    if (+position.id != +linkedInstallations[i].idPosition)
                                        linkedPosition.push(Position.getSingle(linkedInstallations[i].idPosition));
                                }
                            } else
                                linkedPosition = linkedPositions(position);
                            for (var i = 0; i < linkedPosition.length; i++) {
                                dataControl.indexSelectedPosition.push(dataControl.positionList.indexOf(linkedPosition[i]));
                                $scope.data.position.push(linkedPosition[i].name);
                                $scope.data.lampExists.push(true);
                                listWattsInPositions.push(linkedPosition[i].watts);
                            }
                        } else {
                            dataControl.indexSelectedPosition[dataControl.indexPositionToSelect] = dataControl.positionList.indexOf(position);
                            $scope.data.position[dataControl.indexPositionToSelect] = position.name;
                            $scope.data.lampExists[dataControl.indexPositionToSelect] = true;
                            listWattsInPositions[dataControl.indexPositionToSelect] = position.watts;
                        }
                    } else {
                        dataControl.indexSelectedPosition[dataControl.indexPositionToSelect] = dataControl.positionList.indexOf(position);
                        $scope.data.position[dataControl.indexPositionToSelect] = position.name;
                        $scope.data.lampExists[dataControl.indexPositionToSelect] = true;
                        listWattsInPositions[dataControl.indexPositionToSelect] = position.watts;
                    }
                    Map.saveLocation();
                    Map.getGeocoding().then(function (data) {
                        if (data) {
                            $scope.data.address = data.address_components[1].long_name || '';
                            var address = data.formatted_address.split(",");
                            address.shift();
                            $scope.data.addressExtra = address.join(",");
                        }
                    });

                    $scope.lamps = Lamps.getByWatts(dataControl.lampsList, listWattsInPositions);
                    return true;
                }
            }
            return false;
        };
        // When a position is selected check if is in a valid range, and if is already mark as installed to confirm the update action
        $scope.selectPosition = function (position) {
            if (position.distanceMeters <= 100) {
                $rootScope.isFormData = true;
                if (position.installed) {
                    var installation = Install.getSingle(
                            {
                                group: $scope.groups[dataControl.indexSelectedGroup].id,
                                position: position.id
                            }
                    );
                    if (!!installation) {
                        if (dataControl.indexPositionToSelect == 0 || dataControl.indexInstalledPosition.indexOf(dataControl.positionList.indexOf(position)) >= 0) {
                            if (dataControl.indexPositionToSelect == 0) {
                                $ionicPopup.confirm({
                                    title: '',
                                    template: 'Ya se tiene una instalación realizada en esta posición, ¿desea modificarla?',
                                    okText: 'Modificar',
                                    cancelText: 'Cancelar'
                                }).then(function (response) {
                                    if (response) {
                                        if (changePosition(position, installation)) {
                                            $scope.control.editingInstallForPhotos = true;
                                            dataControl.allLampsSelected = true;
                                            $scope.lamps = Lamps.getByWatts(dataControl.lampsList, listWattsInPositions);
                                            dataControl.installedID = [];

                                            // Get lamp info from installation
                                            function lampByID(lamp) {
                                                return lamp.id == this.valueOf();
                                            }
                                            function installByPositionID(install) {
                                                return install.idPosition == this;
                                            }

                                            var linkedInstallations = Install.getLinkedInstallations(installation);
                                            if (linkedInstallations.length == $scope.control.numOfLampsInPosition) {
                                                var arrayToReorderInstallations = [];
                                                for (var i = 0; i < linkedInstallations.length; i++) {
                                                    arrayToReorderInstallations[i] = linkedInstallations.find(installByPositionID, dataControl.positionList[dataControl.indexSelectedPosition[i]].id);
                                                    dataControl.installedID[i] = arrayToReorderInstallations[i].id;
                                                    // Add the lamps installed to show in list
                                                    $scope.lamps.unshift(Lamps.getSingle(arrayToReorderInstallations[i].idLamp));

                                                    $scope.data.photoBefore = arrayToReorderInstallations[i].photoBefore || '';
                                                    $scope.data.photoAfter = arrayToReorderInstallations[i].photoAfter || '';

                                                    $scope.data.removedType[i] = arrayToReorderInstallations[i].removedType;
                                                    $scope.data.removedPower[i] = arrayToReorderInstallations[i].removedPower;
                                                    $scope.data.removedBal[i] = arrayToReorderInstallations[i].removedBal;
                                                }
                                                linkedInstallations = arrayToReorderInstallations;

                                                for (var i = 0; i < linkedInstallations.length; i++) {
                                                    // look for installed lamp in lamps list and set the serial into data lamp
                                                    dataControl.indexSelectedLamp[i] = $scope.lamps.findIndex(lampByID, linkedInstallations[i].idLamp);
                                                    $scope.data.lamp.push($scope.lamps[dataControl.indexSelectedLamp[i]].serial);

                                                    dataControl.indexInstalledPosition[i] = dataControl.indexSelectedPosition[i];
                                                    dataControl.indexInstalledLamps[i] = dataControl.indexSelectedLamp[i];
                                                }
                                            } else {
                                                dataControl.installedID[0] = installation.id;
                                                // Add the lamps installed to show in list
                                                $scope.lamps.unshift(Lamps.getSingle(installation.idLamp));

                                                $scope.data.photoBefore = installation.photoBefore || '';
                                                $scope.data.photoAfter = installation.photoAfter || '';

                                                $scope.data.removedType[0] = installation.removedType;
                                                $scope.data.removedPower[0] = installation.removedPower;
                                                $scope.data.removedBal[0] = installation.removedBal;

                                                dataControl.indexSelectedLamp[0] = $scope.lamps.findIndex(lampByID, installation.idLamp);
                                                $scope.data.lamp.push($scope.lamps[dataControl.indexSelectedLamp[0]].serial);

                                                dataControl.indexInstalledPosition[0] = dataControl.indexSelectedPosition[0];
                                                dataControl.indexInstalledLamps[0] = dataControl.indexSelectedLamp[0];
                                            }

                                            $scope.data.address = installation.address;
                                            $scope.data.addressNumber = installation.addressNumber;
                                            $scope.data.street1 = installation.street1;
                                            $scope.data.street2 = installation.street2;
                                            $scope.data.comments = installation.comments;
                                        }
                                        modalMap.hide();
                                    }
                                });
                            } else {
                                if (changePosition(position)) {
                                    if (dataControl.indexPositionToSelect == 0)
                                        dataControl.installedID = [];
                                }
                                modalMap.hide();
                            }
                        } else {
                            $ionicPopup.alert({
                                title: 'Error',
                                template: 'La posición seleccionada ya tiene una instalación registrada.'
                            });
                        }
                    } else {
                        if (changePosition(position)) {
                            $scope.control.editingInstallForPhotos = false;
                            if (dataControl.indexPositionToSelect == 0)
                                dataControl.installedID = [];
                            $scope.lamps = Lamps.getByWatts(dataControl.lampsList, listWattsInPositions);
                        }
                        modalMap.hide();
                    }
                } else {
                    if (changePosition(position)) {
                        $scope.control.editingInstallForPhotos = false;
                        if (dataControl.indexPositionToSelect == 0)
                            dataControl.installedID = [];
                        $scope.lamps = Lamps.getByWatts(dataControl.lampsList, listWattsInPositions);
                    }
                    modalMap.hide();
                }
            }
        };
        /** ~ **/

        /** Lamp **/
        // Function look for lamp with the serial
        var LampBySerial = function (lamp) {
            return lamp.serial == $scope.data.lamp[this.valueOf()];
        }
        // Lamp codebar scanner
        $scope.lampScanner = function (index) {
            cordova.plugins.barcodeScanner.scan(function (result) {
                if (!result.cancelled && !!result.text) {
                    $scope.data.lamp[index] = result.text;
                    var response = $scope.lampChange(index);

                    if (response != true) {
                        $ionicPopup.alert({
                            title: response.title,
                            template: response.template
                        });
                    }
                }
            }, function (error) {
                $ionicPopup.alert({
                    title: 'Error al leer código de barras',
                    template: 'Ocurrio un error al leer el código de barras: ' + error
                });
            }, {
                prompt : "", // Android
                resultDisplayDuration: 0, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
                disableAnimations: true, // iOS
                disableSuccessBeep: true // iOS and Android
            });
        };
        // When the lamp to install is changed or selected
        $scope.lampChange = function (index) {
            function doLampChange() {
                var response;
                // Get the lamp index, and save it into array (can be more than one lamp on post)
                var indexToAdd = $scope.lamps.findIndex(LampBySerial, index);
                // Validate if lamp wasn't selected, if do clean in the $scope.data
                if (indexToAdd >= 0) {
                    if (dataControl.indexSelectedLamp.indexOf(indexToAdd) < 0) {
                        dataControl.indexSelectedLamp[index] = indexToAdd;
                    } else {
                        $scope.data.lamp[index] = !!$scope.lamps[dataControl.indexSelectedLamp[index]] ? $scope.lamps[dataControl.indexSelectedLamp[index]].serial : '';
                    }
                    response = true;
                } else {
                    var alertTitle = "Código no valido", alertTemplate = "El código de barras " + $scope.data.lamp[index] + " no es una luminaría valida";
                    var lampExists;
                    if (lampExists = Lamps.getAll().find(LampBySerial, index)) {
                        alertTitle = 'Luminaría no valida';
                        if (lampExists.idGroup != $scope.groups[dataControl.indexSelectedGroup].id) {
                            alertTemplate = "La luminaría " + $scope.data.lamp[index] + " esta asignada a otra cuadrilla";
                        } else if (!!lampExists.installed) {
                            alertTemplate = "La luminaría " + $scope.data.lamp[index] + " ya fue instalada";
                        } else {
                            alertTemplate = 'La luminaría ' + $scope.data.lamp[index] + ' no cumple con la potencía requerida para la posición'
                        }
                    }
                    response = {
                        title: alertTitle,
                        template: alertTemplate
                    };
                    $scope.data.lamp[index] = '';
                }
                $scope.$apply();

                // Loop to validate if all the lamps are selected
                for (var i = 0; i < $scope.control.numOfLampsInPosition; i++) {
                    dataControl.allLampsSelected = dataControl.indexSelectedLamp[i] >= 0;
                    if (!dataControl.allLampsSelected) break;
                }

                return response;
            }

            if ($scope.data.photoAfter !== '') {
                $ionicPopup.confirm({
                    title: '',
                    template: 'Si cambia las luminarias <b>se eliminara la foto de después de instalación</b>.',
                    okText: 'Entendido',
                    cancelText: 'Cancelar'
                }).then(function (response) {
                    if (response) {
                        $scope.data.photoAfter = '';
                        return doLampChange();
                    } else if (response === false)
                        $scope.data.lamp[index] = !!$scope.lamps[dataControl.indexSelectedLamp[index]] ? $scope.lamps[dataControl.indexSelectedLamp[index]].serial : '';
                });
            } else
                return doLampChange();
        };
        /** ~ **/

        /** Button Navigation **/
        // Button next/end
        $scope.next = function (watchCall) {
            watchCall = watchCall || false;
            if (watchCall || !$scope.control.buttonDisabled) {
                // If wasn't finished the select position, lamp and take photos phase
                if (!$scope.data.finished) {
                    /* mark as finished if the information if complete */
                    // Check if position are saved and are not the same
                    var validPosition = $scope.control.numOfLampsInPosition == $scope.data.position.length;
                    for (var i = 0; i < $scope.data.position.length && validPosition; i++) {
                        validPosition = !!$scope.data.position[i] || $scope.data.position[i] !== undefined; // Check if the wholes position are selected
                        if (validPosition) {
                            // and look if there is a duplicate position selected
                            var indexOfDuplicate = $scope.data.position.indexOf($scope.data.position[i], (i + 1)); // If -1 means no duplicate value was found
                            // if we found a duplicate then is wrong and we've invalid information
                            if (indexOfDuplicate > 0) {
                                validPosition = false;
                                $scope.data.position[indexOfDuplicate] = '';
                            }
                        }
                    }
                    // Check if the same number of lamps required in post are saved in the object
                    var validLamps = $scope.control.numOfLampsInPosition == $scope.data.lamp.length;
                    for (var i = 0; i < $scope.data.lamp.length && validLamps; i++) {
                        validLamps = !!$scope.data.lamp[i]; // Check if the wholes lamps are selected
                        if (validLamps) {
                            // and look if there is a duplicate lamps selected
                            var indexOfDuplicate = $scope.data.lamp.indexOf($scope.data.lamp[i], (i + 1)); // If -1 means no duplicate value was found
                            // if we found a duplicate then is wrong and we've invalid information
                            if (indexOfDuplicate > 0) {
                                validLamps = false;
                                $scope.data.lamp[indexOfDuplicate] = '';
                            }
                        }
                    }
                    if (!!$scope.data.group && validPosition && validLamps && (!!$scope.data.photoBefore || $scope.control.editingInstallForPhotos)) {
                        $scope.control.buttonDisabled = false;
                        if (!watchCall) {
                            if (!!$scope.data.photoAfter)
                                $scope.control.allPhotosTaked = true;
                            $scope.data.finished = true;
                            Map.initLittleMap();
                        }
                    } else
                        $scope.control.buttonDisabled = true;
                    /* ~ */
                }
                // If the basic info is already registered
                else {
                    if (!watchCall)
                        $scope.data.savesubmited = true;
                    // If the info is complete, send to WS to save it in DB, then clean it to start over
                    if (!!$scope.data.photoAfter || $scope.control.editingInstallForPhotos) {
                        if (!!$scope.data.address && !!$scope.data.addressNumber && !!$scope.data.street1 && !!$scope.data.street2) {
                            $scope.control.buttonDisabled = false;
                            if (!watchCall) {
                                var dataPost = {};
                                // Copy the saved data in the dataPost object
                                angular.copy($scope.data, dataPost);

                                // Status to standby (default value)
                                dataPost.status = Install.statusInstall.STANDBY;
                                // Get the current date of the installation
                                dataPost.installedDate = new Date().getTime();
                                // Get the current position coordinates
                                var currentPosition = Map.currentPosition();
                                dataPost.gpsCoords = '{"latitude":' + currentPosition.latitude + ',"longitude":' + currentPosition.longitude + '}';
                                // rowVersion to same installedDate, this would be updated when the WS sync were done
                                dataPost.rowVersion = dataPost.installedDate;

                                var installationsPromises = [];
                                for (var i = 0; i < $scope.control.numOfLampsInPosition; i++) {
                                    dataPost.id = !!dataControl.installedID[i] ? dataControl.installedID[i] : (!!dataPost.id ? dataPost.id + 1 : (Install.getLastInstallation() != null ? Install.getLastInstallation().id : 0) + 1);
                                    // Get the group id from the array of groups
                                    dataPost.idGroup = $scope.groups[dataControl.indexSelectedGroup].id;
                                    // Get the position (position) id from the array of position
                                    dataPost.idPosition = dataControl.positionList[dataControl.indexSelectedPosition[i]].id;
                                    // Get the lamp id from the array of lamps
                                    dataPost.idLamp = $scope.lamps[dataControl.indexSelectedLamp[i]].id;
                                    // Get the removed lamp info
                                    dataPost.removedType = !!$scope.data.removedType[i] ? $scope.data.removedType[i] : '';
                                    dataPost.removedPower = !!$scope.data.removedPower[i] ? $scope.data.removedPower[i] : '';
                                    dataPost.removedBal = !!$scope.data.removedBal[i] ? $scope.data.removedBal[i] : '';
                                    // If the position (position) has more than one lamp link mark as linked installation
                                    dataPost.linked = +($scope.control.numOfLampsInPosition > 1);
                                    // Clean images if isn't the first saved installation (so we don't overload the database)
                                    if (i > 0) {
                                        dataPost.photoBefore = '';
                                        dataPost.photoAfter = '';
                                    }

                                    installationsPromises.push(Install.setSingle(angular.copy(dataPost), false));
                                }

                                // Save in local database, using the DBService to know when the registers was saved instead of Install service
                                $q.all(installationsPromises).then(function () {
                                    var localPositionLampsPromises = [];
                                    for (var i = 0; i < $scope.control.numOfLampsInPosition; i++) {
                                        if (dataControl.indexInstalledPosition.length > i && dataControl.indexSelectedPosition.indexOf(dataControl.indexInstalledPosition[i]) < 0) {
                                            dataControl.positionList[dataControl.indexInstalledPosition[i]].installed = 0;
                                            dataControl.positionList[dataControl.indexInstalledPosition[i]].rowVersion = dataPost.installedDate;
                                            localPositionLampsPromises.push(Positions.setSingle(dataControl.positionList[dataControl.indexInstalledPosition[i]]));
                                        }
                                        dataControl.positionList[dataControl.indexSelectedPosition[i]].installed = 1;
                                        dataControl.positionList[dataControl.indexSelectedPosition[i]].rowVersion = dataPost.installedDate;
                                        if (dataControl.positionList[dataControl.indexSelectedPosition[i]].id !== 0)
                                            localPositionLampsPromises.push(Positions.setSingle(dataControl.positionList[dataControl.indexSelectedPosition[i]]));

                                        if (dataControl.indexInstalledLamps.length > i && dataControl.indexSelectedLamp.indexOf(dataControl.indexInstalledLamps[i]) < 0) {
                                            $scope.lamps[dataControl.indexInstalledLamps[i]].installed = 0;
                                            $scope.lamps[dataControl.indexInstalledLamps[i]].rowVersion = dataPost.installedDate;
                                            localPositionLampsPromises.push(Lamps.setSingle($scope.lamps[dataControl.indexInstalledLamps[i]]));
                                        }
                                        $scope.lamps[dataControl.indexSelectedLamp[i]].installed = 1;
                                        $scope.lamps[dataControl.indexSelectedLamp[i]].rowVersion = dataPost.installedDate;
                                        localPositionLampsPromises.push(Lamps.setSingle($scope.lamps[dataControl.indexSelectedLamp[i]]));
                                    }

                                    $q.all(localPositionLampsPromises).then(function () {

                                        AppServices.sync();

                                        $ionicPopup.alert({
                                            title: 'Registrado',
                                            template: 'La instalación se guardo correctamente.'
                                        });

                                        // Reset/Clean Data
                                        localStorage.clean('installation');
                                        $state.reload();
                                    }, function () {
                                        $ionicPopup.alert({
                                            title: 'Error',
                                            template: 'No fue posible guardar la instalación, vuelva a intentarlo.'
                                        });
                                    });
                                }).catch(function () {
                                    $ionicPopup.alert({
                                        title: 'Error',
                                        template: 'No fue posible guardar la instalación, vuelva a intentarlo.'
                                    });
                                });
                            }
                        } else if (!watchCall) {
                            // Scroll to form
                            $ionicScrollDelegate.$getByHandle('mainScroll').scrollTo(0, $ionicPosition.position(angular.element(document.getElementsByClassName('info-form')[0])).top, true);
                        } else
                            $scope.control.buttonDisabled = true;
                    } else
                        $scope.control.buttonDisabled = true;
                }
            }
        };
        /** ~ **/

        /** Filter and utils **/
        // Set the boolean to not filter the lamps out of the range
        $scope.showOutOfRange = function () {
            $scope.showAllPosition = true;
        };
        // Filter the ng-repeat showing the list with position in range
        $scope.inRange = function (item) {
            return item.distanceMeters <= 100 || $scope.showAllPosition;
        };
        // Get the linked positions (position) from the given position
        var linkedPositions = function (position) {
            function linkedPosFilter (position) {
                if (!!position.linked) {
                    return position.linked.split(',').indexOf('' + this) >= 0;
                }
                return false;
            }

            return dataControl.positionList.filter(linkedPosFilter, position.id);
        };
        /** ~ **/

        /** Map **/
        // Create map modal
        var modalMap;
        $ionicModal.fromTemplateUrl('modules/selection/views/mapIndex.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            modalMap = modal;
        });

        // Validate of there is information to show the map, or just show the map
        $scope.showMap = function (index) {
            $scope.noMap = (typeof google === 'undefined');
            $scope.showAllPosition = false;
            dataControl.indexPositionToSelect = index;

            if (index > 0)
                $scope.positionsForMap = dataControl.positionList.filter(function (position) {
                    return position.id == this.id || (position.numberOfLamps > 1 && !Install.getSingle({group: $scope.groups[dataControl.indexSelectedGroup].id, position: position.id}));
                }, dataControl.positionList[dataControl.indexInstalledPosition[index]]);
            else
                $scope.positionsForMap = dataControl.positionList;

            if (index == 0 && ($scope.data.lamp.length > 0 || !!$scope.data.photoBefore || !!$scope.data.photoAfter)) {
                $ionicPopup.confirm({
                    title: '',
                    template: 'Ya tiene información ingresada, si selecciona otra ubicación se limpara la información.',
                    okText: 'Entendido',
                    cancelText: 'Cancelar'
                }).then(function (response) {
                    if (response)
                        openMap();
                });
            } else if (index > 0) {
                if (linkedPositions(dataControl.positionList[dataControl.indexSelectedPosition[0]]).indexOf(dataControl.positionList[dataControl.indexSelectedPosition[index]]) >= 0) {
                    $ionicPopup.confirm({
                        title: '',
                        template: 'Se ha indicado que esta posición esta ligada con la primera seleccionada, ¿esta seguro de cambiarla?',
                        okText: 'Cambiar',
                        cancelText: 'Cancelar'
                    }).then(function (response) {
                        if (response)
                            openMap();
                    });
                } else
                    openMap();
            } else
                openMap();
        };

        // Open/show the modal of the map and initiate the map and set markets
        var openMap = function () {
            modalMap.show();

            Map.init(dataControl.positionList);
            var markers = Map.getMarkers();
            dataControl.positionList.forEach(Map.updateDistance);

            for (var i = 0; i < markers.length; i++) {
                markers[i].addListener('click', function () {
                    Map.showInfoWindow(this);
                    for (var x = 0; x < dataControl.positionList.length; x++) {
                        if (x == this.positionIndex)
                            dataControl.positionList[x].clicked = true;
                        else
                            dataControl.positionList[x].clicked = false;
                    }
                    $scope.$apply();
                }, false);
            }
            $scope.$onRootScope('locationChange', function () {
                dataControl.positionList.forEach(Map.updateDistance);
                $scope.$apply();
            });
        };
        /** ~ **/

        /** Camera **/
        // Create camera modal
        var modalCamera;
        if (modalCamera === undefined) {
            $ionicModal.fromTemplateUrl('templates/modal-camara.html', {
                scope: $scope
            }).then(function (camera) {
                modalCamera = camera;
            });
        }

        // Initiate the camera plugin and show the modal
        var replacingPhotoBefore;
        $scope.takePhoto = function () {
            replacingPhotoBefore = false;

            function openCamera() {
                $scope.imgURI = undefined;
                var options = {
                    quality: 40,
                    destinationType: Camera.DestinationType.DATA_URL,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: false,
                    encodingType: Camera.EncodingType.JPEG,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: false,
                    correctOrientation: true,
                    targetWidth: 1200,
                    targetHeight: 1600
                };

                modalCamera.show();

                $cordovaCamera.getPicture(options).then(function (imageData) {
                    $scope.imgURI = "data:image/jpeg;base64," + imageData;
                }, function () {
                    modalCamera.hide();
                });
            }

            if ($scope.control.editingInstallForPhotos) {
                var confirmPopup = $ionicPopup.confirm({
                    title: '',
                    template: 'Se esta editando una instalación, si decide reemplazar las fotos tendrá que tomar ambas fotos ' +
                    '(<b>antes</b> y <b>después</b> de la instalación) y serán reemplazadas de la información ya guardada.' +
                    '<br><br><center>¿Esta seguro de continuar?</center>',
                    okText: 'No',
                    cancelText: 'Si'
                });
                confirmPopup.then(function (response) {
                    if (response === false) {
                        $scope.control.editingInstallForPhotos = false;
                        openCamera();
                    }
                })
            } else {
                if ($scope.data.photoBefore === '' || dataControl.allLampsSelected) {
                    openCamera();
                } else {
                    var confirmPopup = $ionicPopup.confirm({
                        title: '',
                        template: 'Para tomar la foto de <b>después de la instalación</b> debe seleccionar las luminarias.' +
                        '<br><center>Sin embargo puede reemplazar la foto de <b>antes de la instalación</b><br>¿desea reemplazarla?</center>',
                        okText: 'No',
                        cancelText: 'Si'
                    });
                    confirmPopup.then(function (response) {
                        replacingPhotoBefore = !response;
                        if (response === false)
                            openCamera();
                    });
                }
            }
        };

        // Action for modals
        $scope.actionCamera = function (send) {
            if (!send) {
                $scope.imgURI = undefined;
                modalCamera.hide();
            }
            else {
                if ($scope.data.photoBefore === '' || replacingPhotoBefore) {
                    $scope.data.photoBefore = $scope.imgURI;
                    $scope.imgURI = undefined;
                    modalCamera.hide();
                } else if ($scope.data.photoAfter === '') {
                    if ($scope.data.finished) {
                        $scope.data.photoAfter = $scope.imgURI;
                        $scope.imgURI = undefined;
                        modalCamera.hide();
                    } else {
                        var confirmPopup = $ionicPopup.confirm({
                            title: '',
                            template: '<b>Existe una foto <i>antes de la instalación</i></b>, ¿desea reemplazarla con la nueva foto?',
                            okText: 'No',
                            cancelText: 'Si'
                        });
                        confirmPopup.then(function (response) {
                            if (response === false)
                                $scope.data.photoBefore = $scope.imgURI;
                            else if (response)
                                $scope.data.photoAfter = $scope.imgURI;
                            $scope.imgURI = undefined;
                            modalCamera.hide();
                        });
                    }
                } else {
                    var confirmPopup = $ionicPopup.confirm({
                        title: '',
                        template: '¿Desea <b>reemplazar la foto de <i>despues de la instalación</i></b>?',
                        okText: 'No',
                        cancelText: 'Si'
                    });
                    confirmPopup.then(function (response) {
                        if (response === false)
                            $scope.data.photoAfter = $scope.imgURI;
                        $scope.imgURI = undefined;
                        modalCamera.hide();
                    });
                }
            }
        };
        /** ~ **/

        /** Image popover **/
        // Create image modal
        var modalImage;
        $scope.showImage = function (objImg) {
            if (!!objImg) {
                $scope.imageView = objImg;
                $ionicModal.fromTemplateUrl('modules/selection/views/image-popover.html', {
                    scope: $scope,
                    animation: 'slide-in-up'
                }).then(function (modal) {
                    modalImage = modal;
                    modalImage.show();
                });
            }
        };
        $scope.closeImage = function () {
            modalImage.hide();
            modalImage.remove();
        };
        /** ~ **/

        /* Watch variables to evaluate if next/save button must be disabled */
        $scope.$watch('data', function () {
            $scope.next(true);
        }, true);

        /* localStorage functionality so we don't lose the data if the OS kill the app */
        document.addEventListener('pause', function () {
            localStorage.set('installation', {date: new Date().getTime(), data: $scope.data, dataControl: dataControl, control: $scope.control, lamps: $scope.lamps, position: Map.currentPosition()});
        }, false);
        document.addEventListener('resume', recoverLocalStorage, false);
        document.addEventListener('deviceready', recoverLocalStorage, false);
        function recoverLocalStorage(event) {
            var localStorageData = localStorage.get('installation');
            if (!!localStorageData) {
                localStorageData = JSON.parse(localStorageData);

                if (!$rootScope.isFormData && localStorageData.date >= (new Date().getTime() - (24 * 3600000))) {
                    $rootScope.isFormData = !!localStorageData.data.position.length;
                    $scope.data = localStorageData.data;
                    dataControl = localStorageData.dataControl;
                    $scope.control = localStorageData.control;
                    $scope.lamps = localStorageData.lamps;
                    Map.saveLocation(localStorageData.position);

                    if (event.pendingResult) {
                        if (event.pendingResult.pluginServiceName == 'Camera' && event.pendingResult.pluginStatus == 'OK') {
                            if (modalCamera === undefined) {
                                $ionicModal.fromTemplateUrl('templates/modal-camara.html', {
                                    scope: $scope
                                }).then(function (camera) {
                                    modalCamera = camera;
                                    modalCamera.show();
                                });
                            } else
                                modalCamera.show();
                            $scope.imgURI = "data:image/jpeg;base64," + imageData;
                        }
                    }
                }

                localStorage.clean('installation');
            }
        }

        $scope.$on('$destroy', function () {
            // Remove the modals when the page/app close
            if (modalMap !== undefined)
                modalMap.remove();
            if (modalCamera !== undefined)
                modalCamera.remove();
            if (modalImage !== undefined)
                modalImage.remove();
        });
    }
]);