'use strict';

angular.module('app.installation', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
            .state('app.installation', {
                url: '/selection/:idGroup',
                cache: false,
                views: {
                    'content@app': {
                        templateUrl: 'modules/selection/views/index.html',
                        controller: 'selectionController'
                    }
                }
            });
}]);