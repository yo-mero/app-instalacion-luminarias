app.controller('layoutController',
['$scope', '$state', '$ionicPopup', 'loadingScreen', 'AppServices', 'Session', 'localStorage',
    function ($scope, $state, $ionicPopup, loadingScreen, AppServices, Session, localStorage) {
        $scope.changeGroup = function () {
            Session.setUsername("");
            Session.setRowVersion(0);
            $scope.startInstallation();
        };

        $scope.startInstallation = function () {
            localStorage.clean('all');
            $state.go('app.installation', {idGroup: +Session.getUsername()}, {reload: true});
        };

        $scope.goTo = function (module) {
            loadingScreen.show();
            switch (module) {
                case 'installation':
                    if ($state.current.name != 'app.installation') {
                        AppServices.sync().then(function () {
                            loadingScreen.hide();
                            localStorage.clean('all');
                            $state.go('app.installation', {}, {reload: true});
                        }, function (response) {
                            loadingScreen.hide();
                            if (response.confirm) // Retry
                                $scope.goTo('install');
                            else if (response.hasData) { // Go anyway
                                localStorage.clean('all');
                                $state.go('app.installation', {}, {reload: true});
                            }
                        });
                    } else {
                        loadingScreen.hide();
                        $ionicPopup.confirm({
                            title: '',
                            template: 'Ya se encuentra en el modulo de instalación, ¿desea reiniciar el registro de instalación?',
                            cancelText: 'Cancelar',
                            okText: 'Reiniciar'
                        }).then(function (response) {
                            if (response) {
                                localStorage.clean('all');
                                $state.go('app.installation', {}, {reload: true});
                            }
                        });
                    }
                    break;
                default:
                    loadingScreen.hide();
                    break;
            }
        };
    }
]);