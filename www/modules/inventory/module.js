/**
 * @author Gerardo Cuellar (gcuellar) on 3/04/18
 */
angular.module('app.inventory', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
            .state('app.inventory', {
                url: '/inventory/:idGroup',
                cache: false,
                views: {
                    'content@app': {
                        templateUrl: 'modules/inventory/views/index.html',
                        controller: 'inventoryController'
                    }
                }
            })
}]);