/**
 * @author Gerardo Cuellar (gcuellar) on 3/04/18
 */
angular.module('app.inventory')
    .controller('inventoryController',
        ['$scope', '$rootScope', '$state', '$q', '$ionicScrollDelegate', '$ionicModal', 'Group', 'Positions', 'Lamps',
            function ($scope, $rootScope, $state, $q, $ionicScrollDelegate, $ionicModal, Group, Positions, Lamps) {
                var group = Group.getSingle($state.params.idGroup);
                if (group == null)
                    $state.go('index');

                var groupLamps = Lamps.getByFilter(group, []);
                var groupPositions = Positions.getByGroup(group, Positions.getActives());
                $scope.lists = {
                    "lamps": {
                        "name": "Luminarias",
                        "total": groupLamps.length,
                        "itemsLists": getLists(groupLamps, "watts", "W", {installed:1})
                    },
                    "positions": {
                        "name": "Posiciones",
                        "total": groupPositions.length,
                        "itemsLists": getLists(groupPositions, "watts", "W", {installed:1})
                    },
                };
                $scope.filtersListItems = {
                    listFilter: 1,
                    listActive: '',
                    listItemsGroup: ''
                };
                $scope.detailsListItems = [];

                /**
                 * Create multiple lists from a larger list, grouped by property
                 * @param items, larger list
                 * @param property, property to filter
                 * @param concatToGroupValue, string to concat at the end of the value in group
                 * @param countByProperties, Object to search properties and values and add a new property with a counter of elements which satisfies
                 */
                function getLists(items, property, concatToGroupValue, countByProperties) {
                    property = property || "idGroup";
                    concatToGroupValue = concatToGroupValue || "";

                    var listsToReturn = [];

                    var listsOfValuesInProperty = [], propertiesKeysForCount = [];
                    if (!!countByProperties)
                        propertiesKeysForCount = Object.keys(countByProperties);
                    for (var index = 0; index < items.length; index++) {
                        if (!items[index].hasOwnProperty(property)) {
                            listsToReturn.length = 0;
                            break;
                        }

                        var addItemToCounter = false;
                        if (propertiesKeysForCount.length) {
                            for (var k = 0; k < propertiesKeysForCount.length; k++) {
                                if (!items[index].hasOwnProperty(propertiesKeysForCount[k]) || items[index][propertiesKeysForCount[k]] != countByProperties[propertiesKeysForCount[k]])
                                    break;
                            }
                            if (k == propertiesKeysForCount.length)
                                addItemToCounter = true;
                        }

                        var indexOfPropertyInList = listsOfValuesInProperty.indexOf(items[index][property]);
                        if (indexOfPropertyInList < 0) {
                            listsOfValuesInProperty.push(items[index][property]);
                            var list = {
                                "group": items[index][property] + concatToGroupValue,
                                "items": [items[index]]
                            };
                            if (addItemToCounter)
                                list.count = 1;
                            listsToReturn.push(list);
                        } else {
                            listsToReturn[indexOfPropertyInList].items.push(items[index]);
                            if (addItemToCounter)
                                listsToReturn[indexOfPropertyInList].count++;
                        }
                    }

                    return listsToReturn;
                }

                function getDetailsListItems() {
                    function byGroup(itemList) {
                        return itemList.group == this;
                    }

                    function byInstalled(item) {
                        return item.installed == this;
                    }

                    var listItemsByActiveAndGroup = angular.copy($scope.lists[$scope.filtersListItems.listActive].itemsLists.find(byGroup, $scope.filtersListItems.listItemsGroup));

                    switch (+$scope.filtersListItems.listFilter) {
                        default:
                        case 1:
                            break;
                        case 2:
                            listItemsByActiveAndGroup.items = listItemsByActiveAndGroup.items.filter(byInstalled, 0).slice(0);
                            break;
                        case 3:
                            listItemsByActiveAndGroup.items = listItemsByActiveAndGroup.items.filter(byInstalled, 1).slice(0);
                            break;
                    }

                    return listItemsByActiveAndGroup;
                }

                $scope.listChange = function (newList) {
                    if ($scope.lists.hasOwnProperty(newList)) {
                        $scope.filtersListItems.listActive = newList;
                        $scope.filtersListItems.listFilter = 1;
                    }
                };
                for (var key in $scope.lists) {
                    $scope.listChange(key);
                    break;
                }

                $scope.applyFilter = function () {
                    $scope.detailsListItems = getDetailsListItems();
                    $ionicScrollDelegate.$getByHandle('listScroll').scrollTop();
                };

                var modalDetails;
                $scope.showDetails = function (groupList) {
                    $scope.filtersListItems.listItemsGroup = groupList;
                    $scope.detailsListItems = getDetailsListItems();
                    if (modalDetails === undefined) {
                        $ionicModal.fromTemplateUrl('modules/inventory/views/details.html', {
                            scope: $scope,
                            animation: 'slide-in-up'
                        }).then(function (modal) {
                            modalDetails = modal;
                            modalDetails.show();
                        });
                    } else {
                        modalDetails.show();
                    }
                };
                $scope.closeDetails = function () {
                    modalDetails.hide();
                };
                $scope.$on('modal.removed', function() {
                    modalDetails = undefined;
                });
                $scope.$on('$destroy', function () {
                    // Remove the modals when the page/app close
                    if (modalDetails !== undefined)
                        modalDetails.remove();
                });
            }
        ]
    );